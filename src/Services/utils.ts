type Executor<T> = (resolve: (val: T) => any, reject: (e?: Error) => any) => void
export function CancellablePromise<T>(executor: Executor<T>): Promise<T> {
    let _promise = new Promise<T>((resolve, reject) => {

    }) as Promise<T> & { cancel: () => void };
    _promise.cancel = () => { }
    this.__proto__ = _promise
    return this;
}

const promise = new Promise(r => { })
const proxy = new Proxy(promise, {

})