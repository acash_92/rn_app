import { store } from '../Redux/Stores'
import { ScreenNames } from '../Navigators/ScreenNames'
import { NavigationNavigateActionPayload } from 'react-navigation'
import { navigate, getNavigator } from './NavigationService'
import { Store, LooseObject } from 'project-defined-types'

export function structureMappedErrors(errors: Store.UIErrors) {
    /**
     * heirarchy seperated as array
     * param name as normal string
     * array index as ':$index' where $index is the array index as integer 
     * 
     */
    const map: LooseObject = {}
    function recurse(
        keys: string[],
        parentPointer: LooseObject | LooseObject[] | LooseObject,
        val: string) {
        if (keys.length) {
            const [first, ...rest] = keys
            if (/^\:[0-9]+/.test(first)) {
                // if (i === 0) throw new Error("Top most parent can't be an array");
                // tslint:disable-next-line: ban
                const Key = parseInt(first.substr(1), 10);
                parentPointer = (parentPointer || []) as LooseObject[];
                const nextPtr = parentPointer[Key];
                parentPointer[Key] = recurse(rest, nextPtr, val);
                return parentPointer
            } else {
                parentPointer = (parentPointer || {}) as LooseObject;
                const nextPtr = parentPointer[first];
                if (typeof nextPtr === "string") {
                    return parentPointer;
                }
                parentPointer[first] = recurse(rest, nextPtr, val);
                return parentPointer
            }

        } else return [{ message: val }]
    }



    errors.forEach(error => {

        const pattern = error.mapToParams;
        if (!pattern) return;
        recurse(pattern, map, error.message);
    })
    return map;
}

