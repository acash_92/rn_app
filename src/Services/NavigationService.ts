import { NavigationActions, StackActions, NavigationProp, NavigationParams } from 'react-navigation'
import { ScreenNames } from '../Navigators/ScreenNames'
import { store } from '../Redux/Stores'
import { actionMaker } from '../Constants/ActionTypes';
/**
 * The navigation is implemented as a service so that it can be used outside of components, for example in sagas.
 *
 * @see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
 */

// tslint:disable-next-line: no-any
let navigator: NavigationProp<any>
/**
 * This function is called when the RootScreen is created to set the navigator instance to use.
 */
// tslint:disable-next-line: no-any
export function setTopLevelNavigator(navigatorRef: NavigationProp<any>) {
  if (navigatorRef) {
    navigator = navigatorRef;
  }
}


/**
 * Call this function when you want to navigate to a specific route.
 *
 * @param routeName The name of the route to navigate to. Routes are defined in RootScreen using createStackNavigator()
 * @param params Route parameters.
 */
export function navigate(routeName: ScreenNames | ScreenNames[], params?: NavigationParams) {
  store.dispatch(actionMaker.CLEAR_CURRENT_ERRORS());
  navigator.dispatch(
    NavigationActions.navigate({
      routeName: Array.isArray(routeName) ? navigateDeep(routeName) : routeName,
      params,
    })
  )
}

/**
 * Call this function when you want to navigate to a specific route.
 *
 * @param routeName The name of the route to navigate to. Routes are defined in RootScreen using createStackNavigator()
 * @param params Route parameters.
 */
function navigateDeep(routeNames: ScreenNames[], params?: NavigationParams) {

  let props: any;
  for (let i = routeNames.length - 1; i >= 0; --i) {
    const subProps: { [k: string]: any } = {
      routeName: routeNames[i]
    }
    if (props) subProps.action = NavigationActions.navigate(props);
    else {
      subProps.params = params
    }
    props = subProps;
  }
  // store.dispatch(actionMaker.CLEAR_CURRENT_ERRORS());
  // if (props) navigator.dispatch(props);
  return props;
}

/**
 * Call this function when you want to navigate to a specific route AND reset the navigation history.
 *
 * That means the user cannot go back. This is useful for example to redirect from a splashscreen to
 * the main screen: the user should not be able to go back to the splashscreen.
 *
 * @param routeName The name of the route to navigate to. Routes are defined in RootScreen using createStackNavigator()
 * @param params Route parameters.
 */
export function navigateAndReset(routeName: ScreenNames, params?: NavigationParams) {
  store.dispatch(actionMaker.CLEAR_CURRENT_ERRORS());
  // navigateDeep(Array.isArray(routeName) ? routeName : [routeName]);

  navigator.dispatch(
    StackActions.reset({
      index: 0,
      key: null,
      actions: //Array.isArray(routeName) ? navigateDeep(routeName) : 
        [
          NavigationActions.navigate({
            routeName,
            params,
          }),
        ],
    })
  )
}

export function getNavigator() {
  return navigator;
}

export function getCurrentRoute() {
  if (!navigator) return [];
  let nav = navigator.state.nav;
  const route = [];
  while (Array.isArray(nav.routes) && nav.routes.length > 0) {
    route.push(nav.routeName)
    nav = nav.routes[nav.index]
  }
  return route;
}


export function goBack() {
  navigator.dispatch(NavigationActions.back())
}