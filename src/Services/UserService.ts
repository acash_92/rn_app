import Images from "../Theme/Images";
import { POSTjson } from "./HTTPServices"
import { ErrorKey, Config } from "../Constants";
import { Store, GqlBuilder, APITypes, S3SignedURL } from "project-defined-types";
import { AsyncStorage } from 'react-native';
import { Query, Mutation } from "./graphQLServices"
import { KnownPayloads } from "project-defined-types";
import { firebase } from '@react-native-firebase/messaging';
import { DocumentPickerResponse } from "react-native-document-picker";
// import { Config } from '../Config'


export const getToken = () => {
  return AsyncStorage.getItem(Config.localstorage.keys.TOKEN);
}

export const uploadPhoto = async (file: DocumentPickerResponse, uploadKey: string) => {

  const queryObj = new Query();
  queryObj.append({
    operation: "s3signedUploadUrl",
    variables: { fileName: uploadKey, fileType: file.type },
    fields: ["signedRequestUrl", "objectUrl"]
  })
  const { s3signedUploadUrl: { signedRequestUrl, objectUrl } } = await queryObj.exec<{ s3signedUploadUrl: S3SignedURL }>();

  const options = {
    method: 'PUT',
    body: file,
    headers: new Headers({ 'content-type': file.type })
  };
  const response = await fetch(signedRequestUrl, options)
  if (!response.ok) {
    throw new Error(`${response.status}: ${response.statusText}`);
  }
  return objectUrl;


}

export async function login(email: string, password: string) {

  let fcmToken = await AsyncStorage.getItem('fcmToken');
  if (!fcmToken) {
    await firebase.messaging().registerForRemoteNotifications();

    try {
      const fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    } catch (e) {
      console.log(e);
    }
  }
  console.log("fcmToken", fcmToken)
  try {
    const { token } = await POSTjson("/login", { email, password, firebasePushToken: fcmToken || "" });
    await AsyncStorage.setItem(Config.localstorage.keys.TOKEN, token);
  } catch (e) {
    if (e.message in ErrorKey) { throw e; }
    else throw new Error(ErrorKey.NETWORK_ERROR)
  }

}

export async function updateUser({ name, photoURL, password, auth }: KnownPayloads.UpdateCurrentUserPayload) {
  const mutObj = new Mutation();
  mutObj.append({
    operation: "updateCurrentUser",
    variables: {
      update: {
        type: "UpdateUserIN",
        value: { name, picture: photoURL, password, auth },
        required: true
      }
    }
  })
  return await mutObj.exec();
}
export async function logout() {

  // const token = AsyncStorage.getItem(Config.localstorage.keys.TOKEN);
  // const { token } = await POST("/logout",{token})
  await AsyncStorage.removeItem(Config.localstorage.keys.TOKEN);
}

// export async function resetPassword(email: string, password: string) {

//   const { token } = await POST("/resetPassword", { email, password })
//   await AsyncStorage.setItem(Config.localstorage.keys.TOKEN, token);

// }


export async function saveArea({ _id, name, levels, areaCode }: Store.Area): Promise<Store.Area | void> {
  const mutObj = new Mutation()
  const levelsWithJustIds = levels.map(level => {
    return Object.assign({}, level,
      { groups: level.groups.map(group => group._id) },
      // { employees: level.employees.map(employee => employee._id) },
    );
  });
  console.log("###",levels,levelsWithJustIds)
  mutObj.append({
    operation: "saveArea",
    variables: {
      area: {
        type: "AreaIN",
        value: { _id, name, levels: levelsWithJustIds, areaCode },
        required: true
      }
    },
    fields: ['_id', 'name', 'areaCode']
  })

  const { area } = await mutObj.exec();
  return area;
}

export async function saveGroup({ members, ...rest }: Store.Group): Promise<Store.Group | void> {
  const mutObj = new Mutation()
  const userIds = members.map(employee => employee._id);

  mutObj.append({
    operation: "saveGroup",
    variables: {
      group: {
        type: "GroupIN",
        value: { members: userIds, ...rest },
        required: true
      }
    },
    fields: ['_id', 'name']
  })

  const { area } = await mutObj.exec();
  return area;
}



export async function saveUser(user: Store.User): Promise<Store.User | void> {
  const mutObj = new Mutation()
  mutObj.append({
    operation: "saveUser",
    variables: {
      employee: {
        type: "UserIN",
        value: user,
        required: true
      }
    },
    fields: ['_id', 'name', 'picture']
  })

  const { user: updatedUser } = await mutObj.exec();
  return updatedUser;
}

export async function saveLeaveRequest(leaveReq: Store.LeaveRequest) {
  // should add timezone info here
  //should add user details here too --
  const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone || "#" + (new Date()).getTimezoneOffset();
  const { type, startDate, endDate, comment } = leaveReq;
  const mutObj = new Mutation()
  mutObj.append({
    operation: "requestLeave",
    variables: {
      leaveRequest: {
        type: "LeaveRequestIN",
        value: { type, startDate, endDate, comment, timezone },
        required: true
      }
    }
  })

  const { requestLeave } = await mutObj.exec();
  return requestLeave;
}

export function decideOnLeave(id: string, status: Store.LeaveRequest["status"], mutObj = new Mutation()) {
  return mutObj.append({
    operation: "decideOnLeave",
    variables: { id: { type: "String", required: true, value: id }, status: { type: "String", required: true, value: status } }
  })
}

export function getLeaveRequests(params?: KnownPayloads.LeaveRequestParamas, queryObj = new Query()) {

  const variables: GqlBuilder.Variables<KnownPayloads.LeaveRequestParamas> = {};
  if (params && params.itemRange) variables.itemRange = { type: "ItemRange", value: params.itemRange };
  if (params && params.dateRange) variables.dateRange = { type: "DateRange", value: params.dateRange };
  if (params && params.status) variables.status = { type: "String", value: params.status };
  queryObj.append({
    operation: "leaveRequests",
    variables,
    fields: ['_id', 'type', 'startDate', 'endDate', 'days', 'status', "timezone", "comment", "user"]
  });

  return queryObj;
}


export const getEmployees = (queryObj = new Query()) => {
  queryObj.append({
    operation: "employees",
    fields: ['_id', 'name', 'picture', "designation"]
  });

  return queryObj;
}

export const getOneEmployee = (id: string, queryObj = new Query()) => {
  queryObj.append({
    operation: "employee",
    variables: { id },
    fields: ['_id', 'name', 'picture', "designation", "email", "phone", "userType"]
  });

  return queryObj;
}

export const getOneArea = (id: string, queryObj = new Query()) => {
  queryObj.append({
    operation: "area",
    variables: { id: { value: id, required: true, type: "String" } },
    fields: ['_id', 'name', 'areaCode', {
      'levels':
        [
          { 'groups': ['_id'] },
          // { 'employees': ['_id'] },
          'shouldComment',
          'timeout']
    }]
  });

  interface ReturnType { area: Store.Area };
  return queryObj;
}

export const getOneGroup = (id: string, queryObj = new Query()) => {
  queryObj.append({
    operation: "group",
    variables: { id: { value: id, required: true, type: "String" } },
    fields: ['_id', 'name', 'description', { members: ["_id"] }]
  });

  interface ReturnType { group: Store.Group };
  return queryObj;
}


export const getOneLeaveReq = (id: string, queryObj = new Query()) => {
  queryObj.append({
    operation: "leaveRequest",
    variables: { id: { value: id, required: true, type: "String" } },
    fields: ['_id', 'type', 'startDate', "endDate", 'days', 'status', 'comment', "timezone"]
  });
  return queryObj;
}

export function getCurrentUser(queryObj = new Query()) {
  return getOneEmployee("");
}

export const getGroups = (queryObj = new Query()) => {
  queryObj.append({
    operation: "groups",
    fields: ['_id', 'name']
  });
  return queryObj;
}

export const getAreas = (queryObj = new Query()/*  */) => {
  queryObj.append({
    operation: "areas",
    fields: ['_id', 'name', 'areaCode']
  });

  // if queryObj;
  return queryObj;
}
export const getLog = (queryObj = new Query()) => {

}



//delete ops

export const deleteLeaveReq = (id: string, mutObj = new Mutation) => {
  return mutObj.append({
    operation: "deleteLeaveReq",
    variables: { id: { type: "String", required: true, value: id } }
  })
}

export const deleteUser = (id: string, mutObj = new Mutation) => {
  return mutObj.append({
    operation: "deleteUser",
    variables: { id: { type: "String", required: true, value: id } }
  })
}


export const deleteArea = (id: string, mutObj = new Mutation) => {
  return mutObj.append({
    operation: "deleteArea",
    variables: { id: { type: "String", required: true, value: id } }
  })
}

export const deleteGroup = (id: string, mutObj = new Mutation) => {
  return mutObj.append({
    operation: "deleteGroup",
    variables: { id: { type: "String", required: true, value: id } }
  })
}