
import { Config } from "../Constants";

const uri = `${Config.api_server.disable_https ? "http" : "https"}://${Config.api_server.host}:${Config.api_server.port}`

export const POSTjson = async (path: string, obj?: { [key: string]: string }) => {
    const resp = await fetch(
        `${uri}${path}`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        }
    );
    if (resp.ok) {
        return resp.json();
    } else {
        throw (await resp.json())
    }
}


// export const POSTmultipart = async (url: string, formData: FormData) => {
//     const resp = await fetch(
//         url,
//         {
//             method: 'POST',
//             body: formData
//         }
//     );
//     if (resp.ok) {
//         return await resp.text();
//     } else {
//         throw (await resp.text())
//     }
// }