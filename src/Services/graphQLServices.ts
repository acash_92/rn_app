
import { Config, ErrorKey } from "../Constants";
import { AsyncStorage } from 'react-native';
import { CANCEL } from 'redux-saga'
// import { createHttpLink } from 'apollo-link-http';
import * as GQBuilder from 'gql-query-builder'

interface QueryBuilderOptions {
    operation: string;
    fields?: Array<string | object>;
    // tslint:disable-next-line: no-any
    variables?: any;
}

interface PromiseWithSagaCancel<T> extends Promise<T> {
    [key: string]: () => void
}

type PromiseOptionalCancellable<T> = PromiseWithSagaCancel<T> | Promise<T>

const uri = (Config.api_server.disable_https ? "http" : "https")
    + "://"
    + Config.api_server.host
    + ":"
    + Config.api_server.port
    + Config.api_server.gql_path;


const getFetch = async (query: string, variables: { [key: string]: any }): Promise<{ req: Promise<Response>, cancel: () => void }> => {
    const controller = new AbortController();
    const signal = controller.signal;
    const token = await AsyncStorage.getItem(Config.localstorage.keys.TOKEN);
    const fetchPromise = fetch(uri, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            [Config.api_server.auth_token_header_field]: token || ''
        },
        body: JSON.stringify({
            query,
            variables,
        }),
        signal
    })
    // Object.assign(fetchPromise, { cancel: () => controller.abort.bind(controller) });
    return { req: fetchPromise, cancel: controller.abort.bind(controller) }

}

export class Query {
    protected queryList: QueryBuilderOptions[] = []
    protected getQueryBuilder() {
        return GQBuilder.query.bind(GQBuilder);
    }
    append(option: QueryBuilderOptions) {
        if (!option.variables) {
            option.variables = {};
            this.queryList.unshift(option); // a bug with query builder. queries without variables should come first.
        } else {
            this.queryList.push(option);
        }
        return this;
    }
    cancel() {
        throw new Error("cancel called before executing");
    }
    async exec<T>(): Promise<T> {

        const { query, variables } = this.getQueryBuilder()(this.queryList);
        console.log("qString", query, variables)
        const request = await getFetch(query, variables)
        this.cancel = () => {
            if (request.cancel) { request.cancel(); }
        }

        const returnPromise = new Promise<T>(async (resolve, reject) => {
            const { data, errors }: { data: T, errors: [{ message: string }] } = await (await request.req).json();
            console.log("#resp", data, errors)
            if (errors) {
                const knownErrors: Error[] = []
                errors.forEach(({ message }) => {
                    try {
                        const deserialized = message.split(',');

                        deserialized.forEach(errorKey => {

                            if (errorKey in ErrorKey) knownErrors.push(new Error(errorKey));
                            else throw new Error("Unknown Errors returned")
                        });
                        return;
                    } catch (e) {
                        knownErrors.push(new Error(ErrorKey.API_ERROR));
                    }
                })
                reject(knownErrors)
            }
            resolve(data);
        })
        Object.assign(returnPromise, { [CANCEL]: request.cancel })
        return returnPromise;
    }
}

export class Mutation extends Query {
    protected getQueryBuilder() {
        return GQBuilder.mutation.bind(GQBuilder);
    }
  
}
