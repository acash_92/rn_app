/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

// tslint:disable-next-line: no-default-export
export default {
  logo: require('../Assets/Images/TOM.png'),
  logo_long: require('../Assets/Images/logo.png'),
}
