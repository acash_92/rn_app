/**
 * This file defines the base application styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */

// tslint:disable-next-line: no-default-export
import { ViewStyle, Platform, StyleSheet } from "react-native";
import Colors from "./Colors";
import autoMergeLevel1 from "redux-persist/es/stateReconciler/autoMergeLevel1";

const APPBAR_HEIGHT = Platform.select({
  ios: 44,
  android: 56,
  default: 64,
});

export const ScreenStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start"
  } as ViewStyle,

  row: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "stretch",
    width: "100%",
    padding: 2
  },
  stack: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch",
    padding: 2
  },
  floatContainer: {
    alignItems: "flex-start",
    flexWrap: "wrap"
  },
  floatItem: {
    minWidth: "30%",
    width: "auto",
    backgroundColor: Colors.selectionHighlight1,
    margin: 1
  },
  listItem: {
    padding: 5, minHeight: 40,
    borderColor: Colors.edges,
    borderBottomWidth: 1,
  },
  selectedItem: {
    backgroundColor: Colors.selectionHighlight1
  },
  heading: {
    fontSize: 18,
    color: Colors.textHeading
  },
  subText: {
    fontSize: 14,
    color: Colors.text
  },
});

export const AppBarStyle = StyleSheet.create({
  menuButtonStyle: { width: 30, height: 30, margin: 5 },
  menuProfileIcon: { width: 35, height: 35 },
  menuProfileContainer: {
    overflow: "hidden",
    width: 35, height: 35, borderRadius: 18, borderWidth: 1,
    borderColor: `${Colors.edges}`
  },
  menuLogo: { height: 35, width: "auto" },
  container: {
    flexDirection: 'row',
    width: "100%",
    height: APPBAR_HEIGHT,
    justifyContent: "space-between",
    backgroundColor: Colors.primaryBackground,
    alignItems: "center",
    padding: 15
  },
});

