import { faFastForward } from "@fortawesome/free-solid-svg-icons";

/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

// tslint:disable-next-line: no-default-export
export default {
  transparent: 'rgba(0,0,0,0)',
  //Example colors:
  text: '#212529',
  textHeading:'#444',
  primaryBackground: '#EE6E73',//#007bff',
  primaryForeground:'#fff',
  disabledBgColor: 'rgba(0,0,0,0.3)',
  primaryTint:'#C05060',
  secondaryBackground: '#EE6E73',//#007bff',
  secondaryForeground:'#fff',
  success: '#28a745',
  error: '#dc3545',
  warning: '#f90',
  info: '#0bf',
  edges:'#cccccc',
  selectionHighlight1:'#e9e9ef'
}
