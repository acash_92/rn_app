import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { SplashScreen } from '../Containers/SplashScreen/SplashScreen'
import { SettingsScreen } from '../Containers/SettingsScreen'
import { AdminDrawer, EmployeeDrawer } from './DrawerNavigation'
import { ScreenNames, ScreenLabels } from './ScreenNames'
import { LoginScreen } from '../Containers/LoginScreen/LoginScreen'
import { Colors } from '../Theme'


// tslint:disable-next-line: no-any
function withoutAppBar(Screen: React.ComponentType<any>) {
  return {
    screen: Screen,
    navigationOptions: {
      header: null
    }
  }
}
/**
 * The root screen contains the application's navigation.
 *
 * @see https://reactnavigation.org/docs/en/hello-react-navigation.html#creating-a-stack-navigator
 */
const StackNavigator = createStackNavigator(
  {
    [ScreenNames.SplashScreen]: withoutAppBar(SplashScreen),
    [ScreenNames.AdminScreenStack]: withoutAppBar(AdminDrawer),
    [ScreenNames.EmployeeScreenStack]: withoutAppBar(EmployeeDrawer),
    [ScreenNames.SettingsScreen]: {
      screen: SettingsScreen, navigationOptions: {
        title: ScreenLabels[ScreenNames.SettingsScreen],
        headerStyle: {
          backgroundColor: Colors.primaryBackground,
        },
        headerTintColor: Colors.primaryTint,
        headerTitleStyle: {
          fontWeight: 'bold',
        }
      }
    },
    [ScreenNames.LoginScreen]: LoginScreen,
  },
  {
    // By default the application will show the splash screen
    initialRouteName: ScreenNames.SplashScreen,
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: 'float',
  }
)

// tslint:disable-next-line: no-default-export
export default createAppContainer(StackNavigator);
