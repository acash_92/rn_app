

import DashboardScreen from '../../Containers/DashboardScreen';
import {PendingLeaveReqScreen} from '../../Containers/LeaveApprovalScreen';
import { AlarmHistoryScreen } from '../../Containers/AlarmHistoryScreen';
import { SettingsScreen } from '../../Containers/SettingsScreen';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { MenuBar } from './MenuBar';
import { DrawerComponent } from './DrawerComponent';
import { Dimensions, View, ActivityIndicator } from 'react-native';
import * as React from 'react';
import { ScreenNames } from '../ScreenNames';
import { faBars, faBell, faCommentAlt, IconDefinition } from '@fortawesome/free-solid-svg-icons'
import { AreaStack, EmployeeStack, EmpGroupStack, LeaveRequestStack } from './ThirdLayerNav'
import { store } from '../../Redux/Stores';


export interface DrawerScreenDescriptor {
    icon?: IconDefinition,
    screenName: ScreenNames,
    // tslint:disable-next-line: no-any
    screen?: React.ComponentType<any> // will be a child of drawer if provided
    showIf?: 'admin' | 'employee'

}

const drawerScreenList: DrawerScreenDescriptor[] = [
    {
        icon: faBars,
        screenName: ScreenNames.Dashboard,
        screen: withAppBar(DashboardScreen),  // only its children will have a drawer in it
        showIf: 'admin'
    },

    {
        icon: faBars,
        screenName: ScreenNames.AreaScreen,
        screen: AreaStack,  // only its children will have a drawer in it
        showIf: 'admin'
    },


    {
        icon: faBars,
        screenName: ScreenNames.EmployeeScreen,
        screen: EmployeeStack,  // only its children will have a drawer in it
        showIf: 'admin'
    },

    {
        icon: faBars,
        screenName: ScreenNames.EmpGroupScreen,
        screen: EmpGroupStack,  // only its children will have a drawer in it
        showIf: 'admin'
    },

    {
        icon: faBars,
        screenName: ScreenNames.LeaveApprovalScreen,
        screen: withAppBar(PendingLeaveReqScreen),  // only its children will have a drawer in it
        showIf: 'admin'
    },
    // Employee screens


    {
        icon: faBars,
        screenName: ScreenNames.MyAcknowledgements,
        screen: withAppBar(DashboardScreen),  // only its children will have a drawer in it
        showIf: 'employee'
    },


    {
        icon: faBars,
        screenName: ScreenNames.LeaveScreen,
        screen: LeaveRequestStack,  // only its children will have a drawer in it
        showIf: 'employee'
    },
    {
        icon: faBars,
        screenName: ScreenNames.AlarmHistory,
        screen: withAppBar(AlarmHistoryScreen),  // only its children will have a drawer in it
        showIf: 'employee'
    },
    // {
    //     icon: faBars,
    //     screenName: ScreenNames.SettingsScreen,
    //     screen: withAppBar(SettingsScreen),  // only its children will have a drawer in it
    // },

];

// tslint:disable-next-line: no-any
function withAppBar(containerComponent: React.ComponentType<any>) {
    return createStackNavigator({
        //All the screen from the Screen1 will be indexed here
        ScreenStack: {
            screen: containerComponent,
            navigationOptions: {
                header: props => <MenuBar {...props} />,
            },
        },
    })
}

function getFilteredDrawerList(drawerList: DrawerScreenDescriptor[], type: DrawerScreenDescriptor["showIf"]) {

    return drawerList.filter(drawerItem => {
        if (drawerItem.showIf === type || !drawerItem.showIf) return true;
        return false;
    })
}
function generateDrawerScreenConfig(drawerList: DrawerScreenDescriptor[], type: DrawerScreenDescriptor["showIf"]) {
    // tslint:disable-next-line: no-any
    const configObj: { [key in ScreenNames]?: any } = {};
    getFilteredDrawerList(drawerList, type).forEach(item => {
        if (item.screen) {
            configObj[item.screenName] = item.screen
        }
    })
    return configObj;
}


export const AdminDrawer = createDrawerNavigator(generateDrawerScreenConfig(drawerScreenList, "admin"), {
    //For the Custom sidebar menu we have to provide our CustomSidebarMenu
    contentComponent: (props) => {
        return <DrawerComponent {...props} drawerScreenList={getFilteredDrawerList(drawerScreenList, "admin")} firstScreen={ScreenNames.AdminScreenStack} />
    },
    //Sidebar width
    drawerWidth: Dimensions.get('window').width - 130,
});


export const EmployeeDrawer = createDrawerNavigator(generateDrawerScreenConfig(drawerScreenList, "employee"), {
    //For the Custom sidebar menu we have to provide our CustomSidebarMenu
    contentComponent: (props) => {
        return <DrawerComponent {...props}  drawerScreenList={getFilteredDrawerList(drawerScreenList, "employee")} firstScreen={ScreenNames.EmployeeScreenStack} />
    },
    //Sidebar width
    drawerWidth: Dimensions.get('window').width - 130,
});