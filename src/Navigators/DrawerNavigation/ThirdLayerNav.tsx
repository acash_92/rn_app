import { LeaveScreen } from '../../Containers/LeaveScreen'
import { ApplyLeaveScreenDef } from '../../Containers/LeaveScreen/ApplyLeaveScreen'
import { AreaScreen } from '../../Containers/AreaScreen';
import { AddAreaScreenDef } from '../../Containers/AreaScreen/AddAreaScreen'
import { ViewAreaScreenDef } from '../../Containers/AreaScreen/ViewAreaScreen'
import { EmployeeScreen } from '../../Containers/EmployeeScreen';
import { AddEmployeeScreenDef } from '../../Containers/EmployeeScreen/AddEmployeeScreen'
import { ViewEmployeeScreenDef } from '../../Containers/EmployeeScreen/ViewEmployeeScreen'
import { GroupScreen } from '../../Containers/EmpGroupScreen';
import { AddGroupScreenDef } from '../../Containers/EmpGroupScreen/AddGroupScreen'
import { ViewGroupScreenDef } from '../../Containers/EmpGroupScreen/ViewGroupScreen'
import { Colors } from '../../Theme'
import { ScreenNames, ScreenLabels } from '../ScreenNames'
import { createStackNavigator, NavigationStackProp, HeaderProps } from 'react-navigation-stack';
import { MenuBar } from './MenuBar';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { ThirdLayerAppbar } from 'project-defined-types';
import { actionMaker } from '../../Constants/ActionTypes';
import { ActionType } from '@redux-saga/types';
import { store } from '../../Redux/Stores';
import { AppBarStyle } from '../../Theme/ApplicationStyles';
import { ActionBundle } from '../../Redux/Reducers';
import { NavigationScreenConfigProps } from 'react-navigation';
import { NavigationDrawerProp } from 'react-navigation-drawer';


type NextScreen = ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle>
// tslint:disable-next-line: no-any
function stackConfigGenerator(MainScreen: React.ComponentType<any>, ...followingScreens: NextScreen[]) {
    const screenconfig = {
        "ParentScreen": {
            screen: MainScreen,
            navigationOptions: {
                header: (props: HeaderProps) => <MenuBar {...props} />,
            }
        }
    };

    followingScreens.forEach(item => {
        Object.assign(screenconfig, {
            [item.screenName]: {
                screen: item.screen,
                navigationOptions: ({ navigation }: { navigation: NavigationDrawerProp }) => ({
                    title: navigation.getParam("title") || ScreenLabels[item.screenName],
                    headerRight: (
                        item.menuBarButtons.map(menuItem => (<TouchableOpacity
                            style={[AppBarStyle.menuButtonStyle]}
                            onPress={() => menuItem.event(store.dispatch)}
                        >
                            <FontAwesomeIcon icon={menuItem.icon} color={Colors.primaryForeground} />
                        </TouchableOpacity>)
                        )

                    ),
                })
            }
        })
    })

    return createStackNavigator(
        screenconfig,
        {
            // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
            headerMode: 'float',
            defaultNavigationOptions: {
                headerStyle: {
                    backgroundColor: Colors.primaryBackground,
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }
        }
    )
}
export const AreaStack = stackConfigGenerator(AreaScreen, ViewAreaScreenDef, AddAreaScreenDef)
export const EmployeeStack = stackConfigGenerator(EmployeeScreen, ViewEmployeeScreenDef, AddEmployeeScreenDef)
export const EmpGroupStack = stackConfigGenerator(GroupScreen, ViewGroupScreenDef, AddGroupScreenDef)
export const LeaveRequestStack = stackConfigGenerator(LeaveScreen, ApplyLeaveScreenDef)