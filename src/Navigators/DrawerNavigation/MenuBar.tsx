import * as  React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { NavigationRoute } from 'react-navigation';
import { NavigationStackProp } from 'react-navigation-stack';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBars, faBell, faCommentAlt } from '@fortawesome/free-solid-svg-icons'
import { Images, AppBarStyle } from '../../Theme';
import { ScreenNames } from '../ScreenNames';
import { RoundImg } from '../../Components'
import { store } from '../../Redux/Stores'

const { menuButtonStyle, menuLogo, menuProfileContainer, menuProfileIcon, container } = AppBarStyle
export interface Props {
    navigation: NavigationStackProp<NavigationRoute>;
};



// class HambergerToggle extends React.Component<Props> {
//     //Structure for the navigatin Drawer
//     toggleDrawer = () => {
//         //Props to open/close the drawer
//         this.props.navigation.toggleDrawer();
//     };
//     render() {
//         return (
//             <View style={{ flexDirection: 'row' }}>
//                 <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
//                     {/*Donute Button Image */}
//                     <FontAwesomeIcon icon={faBars} color="black"
//                         style={menuButtonStyle}
//                     />
//                 </TouchableOpacity>
//             </View>
//         );
//     }
// }

export class MenuBar extends React.Component<Props> {
    //Structure for the navigatin Drawer
    squareBtn = () => { };
    showNotifications = () => { };
    showProfile = () => {
        this.props.navigation.navigate(ScreenNames.SettingsScreen)
    };
    openDrawer = () => {
        //Props to open/close the drawer
        this.props.navigation.openDrawer();
    };
    render() {

        const { user } = store.getState();
        const dp = user?.picture ? { uri: user.picture } : Images.logo;
        return (
            <View style={[container, { backgroundColor: 'white' }]}>
                <View style={{ flexDirection: "row", flexGrow: 1, alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.openDrawer.bind(this)}
                        style={menuButtonStyle}>
                        {/*Donute Button Image */}
                        <FontAwesomeIcon icon={faBars} color="black"
                            style={menuButtonStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showProfile.bind(this)}
                        style={{ ...menuButtonStyle, width: (container.height as number) * 3 }}>
                        <Image style={menuLogo} source={Images.logo_long} resizeMode={'contain'} />

                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row", flexGrow: 1, justifyContent: "flex-end", alignItems: 'center' }}>
                    <TouchableOpacity onPress={this.showNotifications.bind(this)} style={menuButtonStyle}>
                        <FontAwesomeIcon icon={faCommentAlt} color="black"
                            style={menuButtonStyle}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.squareBtn.bind(this)} style={menuButtonStyle}>
                        <FontAwesomeIcon icon={faBell} color="black"
                            style={menuButtonStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showProfile.bind(this)} >
                        {/* <Image style={menuProfileIcon} source={Images.logo} resizeMode={'contain'} /> */}

                        <RoundImg radius={18} height={35} source={dp} style={{borderWidth:2}}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}


