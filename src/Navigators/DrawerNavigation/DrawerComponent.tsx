//This is an example code for Navigation Drawer with Custom Side bar//
import * as  React from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import { Images } from '../../Theme';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { getCurrentRoute, navigate, navigateAndReset } from '../../Services/NavigationService'
import { DrawerScreenDescriptor } from "./index"
import { ScreenLabels, ScreenNames } from '../ScreenNames';
import { store } from '../../Redux/Stores';
import { faCaretRight, faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { DrawerContentComponentProps } from 'react-navigation-drawer/lib/typescript/src/types';



// tslint:disable-next-line: no-any
interface Props extends DrawerContentComponentProps {
    drawerScreenList: DrawerScreenDescriptor[],
    firstScreen: ScreenNames
}
// tslint:disable-next-line: no-any
export const DrawerComponent: React.FC<Props> = ({ drawerScreenList, firstScreen, navigation }) => {
    const user = store.getState().user;
    const subRoute = getCurrentRoute().pop();

    const dp = user?.picture ? { uri: user.picture } : Images.logo;
    return (
        <View style={styles.sideMenuContainer}>
            {/*Top Large Image */}
            <Image
                source={dp}
                style={styles.sideMenuProfileIcon}
            />
            {/*Divider between Top Image and Sidebar Option*/}
            <View
                style={{
                    width: '100%',
                    height: 1,
                    backgroundColor: '#e2e2e2',
                    marginTop: 15,
                }}
            />
            {/*Setting up Navigation Options from option array using loop*/}
            <View style={{ width: '100%' }}>
                {drawerScreenList.map((item, key) => (
                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingTop: 10,
                            paddingBottom: 10,
                            backgroundColor: subRoute === (item.screenName) ? '#e0dbdb' : '#ffffff',
                        }}

                        onPress={() => {
                            if (subRoute === item.screenName) {
                                return navigation.closeDrawer();
                            }
                            if (key) navigate(item.screenName);
                            else navigateAndReset(firstScreen); // first item should reset and go to main screen
                        }}
                        key={key}>
                        <View style={{ marginRight: 10, marginLeft: 20 }}>
                            {item.icon ? <FontAwesomeIcon icon={item.icon} /> : null}
                        </View>
                        <Text
                            style={{
                                fontSize: 15,
                                color: subRoute === (item.screenName) ? 'red' : 'black',
                            }}
                        >
                            {ScreenLabels[item.screenName]}
                        </Text>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    sideMenuContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: 0,
    },
    sideMenuProfileIcon: {
        resizeMode: 'center',
        width: 150,
        height: 150,
        marginTop: 20,
        borderRadius: 150 / 2,
    },
});

