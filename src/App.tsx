import * as React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './Redux/Stores'
import {RootScreen} from './Containers/Root/RootScreen'
import {unsubscribe} from './Services/fcmService'

// tslint:disable-next-line: no-default-export
export default class App extends React.Component {
  componentWillUnmount(){
    unsubscribe()
  }
  render() {
    return (
      /**
       * @see https://github.com/reduxjs/react-redux/blob/master/docs/api/Provider.md
       */
      <Provider store={store}>

        <PersistGate loading={null} persistor={persistor}>
          <RootScreen />
        </PersistGate>
      </Provider>
    )
  }
}
