import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from "./Reducers/index";
import rootSaga from '../Sagas';

import { composeWithDevTools } from 'remote-redux-devtools';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const persistConfig = {
  key: 'root',
  storage,
  /**
   * Blacklist state that we do not need/want to persist
   */
  blacklist: ['waiting', 'currentErrors']

}

const middleware = []
const enhancers = []

// Connect the sagas to the redux store
const sagaMiddleware = createSagaMiddleware()
middleware.push(sagaMiddleware)

enhancers.push(composeWithDevTools(applyMiddleware(...middleware)))

const persistedReducer = persistReducer(persistConfig, rootReducer)
export const store = createStore(persistedReducer, compose(...enhancers));
export const persistor = persistStore(store);
sagaMiddleware.run(rootSaga)