
import { ActionTypes } from "../../Constants/ActionTypes";
// import { KnownPayloads } from "../KnownPayloads";
import { FlatReducer } from '.'
import { KnownPayloads,Store } from 'project-defined-types';



export const errorReducers: FlatReducer = (state, { type, payload }) => {

  if (!state) return false;

  switch (type) {
    case ActionTypes.SHOW_CURRENT_UI_ERROR: {
      const newState = Object.assign({}, state); 
      if (!payload) throw new Error("Fail action can't be without Errors");
      newState.currentErrors = [...payload as KnownPayloads.UIErrors]
      return newState;
    }
    case ActionTypes.CLEAR_CURRENT_ERRORS: {
      const { currentErrors, ...newState } = state; // delete currentErrors from state
      return newState;
    }
    default:
      return false;
  }
}

function flattenError(e: Error): Error {
  return {
    name: e.name,
    message: e.message,
    stack: e.stack
  }
}