
import { ActionTypes } from "../../Constants/ActionTypes";
// import { KnownPayloads } from "../KnownPayloads";
import { MonoReducer } from '.'
import { KnownPayloads ,Store} from 'project-defined-types';

export const addAreaDirtyState: Store["addAreaDirty"] = {
  _id: '',
  name: '',
  areaCode:'',
  levels: [],
  shouldUpdate: false
}

export const empDirtyState: Store["employeeDirty"] = {
  _id: '',
  name: '',
  designation: '',
  email: '',
  picture: '',
  phone: '',
  userType: "EMPLOYEE",
  shouldUpdate: false
}
export const groupDirtyState: Store["addGroupDirty"] = {
  description: "",
  _id: "",
  members: [],
  name: "",
  shouldUpdate: false
}
export const leaveDirtyState: Store["applyLeaveDirty"] = {
  _id: "",
  type: "annual",
  startDate: new Date(),
  endDate: new Date(),
  days: 1,
  status: "pending",
  shouldUpdate: false
}

export const addAreaDirtyReducer: MonoReducer<"addAreaDirty"> = (state, { type, payload, errors }) => {
  switch (type) {

    case ActionTypes.SET_DIRTY_ADD_AREA: {
      if (payload) {
        return { ...addAreaDirtyState, ...state, ...payload as Store.Area };
      } else return { ...addAreaDirtyState, ...state }
    }
    case ActionTypes.CLEAR_DIRTY_AREA: {
      return { ...addAreaDirtyState };
    }

    default: {
      return false

    }
  }

  // return store;
}


export const employeeDirtyReducer: MonoReducer<"employeeDirty"> = (state, { type, payload, errors }) => {
  switch (type) {

    case ActionTypes.SET_DIRTY_EMPLOYEE: {
      if (payload) {
        return { ...empDirtyState, ...state, ...payload as Store.Employee };
      } else return { ...empDirtyState, ...state }
    }
    case ActionTypes.CLEAR_DIRTY_EMPLOYEE: {
      return { ...empDirtyState };
    }

    default: {
      return false

    }
  }

}


export const groupDirtyReducer: MonoReducer<"addGroupDirty"> = (state, { type, payload, errors }) => {
  switch (type) {

    case ActionTypes.SET_DIRTY_GROUP: {
      if (payload) {
        return { ...groupDirtyState, ...state, ...payload as Store.Group };
      } else return { ...groupDirtyState, ...state }
    }
    case ActionTypes.CLEAR_DIRTY_GROUP: {
      return { ...groupDirtyState };
    }

    default: {
      return false

    }
  }

}



export const leaveDirtyReducer: MonoReducer<"applyLeaveDirty"> = (state, { type, payload, errors }) => {
  switch (type) {

    case ActionTypes.SET_DIRTY_LEAVE_REQUEST: {
      if (payload) {
        return { ...leaveDirtyState, ...state, ...payload as Store.LeaveRequest };
      } else return { ...leaveDirtyState, ...state }
    }
    case ActionTypes.CLEAR_DIRTY_LEAVE_REQUEST: {
      return { ...leaveDirtyState };
    }

    default: {
      return false

    }
  }

  // return store;
}

