
import { Store } from 'project-defined-types'
import { ActionTypes } from "../../Constants/ActionTypes";
// import { KnownPayloads } from "../KnownPayloads";
import { FlatReducer } from '.'


export const apiResponseReducers: FlatReducer = (state, { type, payload, errors }) => {
  if (!state) return false;
  switch (type) {

    case ActionTypes.LOGIN_SUCCESS: {
      return { ...state, user: { ...payload as Store.User } };
    }
    case ActionTypes.WAITING_RESPONSE: {
      return { ...state, waiting: true };
    }
    case ActionTypes.CLEAR_RESPONSE_WAIT: {
      return { ...state, waiting: false };
    }
    case ActionTypes.CLEAR_SESSION: {
      const { user, ...newState } = state;
      return newState;
    }


    case ActionTypes.SET_EMPLOYEE_LIST: {
      return { ...state, employees: payload as Store.Employee[] }
    }
    case ActionTypes.SET_GROUP_LIST: {
      return { ...state, groups: payload as Store.Group[] }
    }
    case ActionTypes.SET_AREA_LIST: {
      return { ...state, areas: payload as Store.Area[] }
    }
    
    case ActionTypes.SET_LEAVE_REQUEST_LIST: {
      return { ...state, leaveRequests: payload as Store.LeaveRequest[] }
    }



    default: {
      return false

    }
  }

  // return store;
}

