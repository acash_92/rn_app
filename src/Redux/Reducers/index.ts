import { ActionTypes } from "../../Constants/ActionTypes";
import { apiResponseReducers } from './apiResponseReducers';
import {
  addAreaDirtyReducer, addAreaDirtyState,
  employeeDirtyReducer, empDirtyState,
  groupDirtyReducer, groupDirtyState,
  leaveDirtyReducer, leaveDirtyState,
} from './dirtyStateReducers';
import { errorReducers } from './errorReducers';
// import { combineReducers } from 'redux';
import { Store, KnownPayloads } from 'project-defined-types'


export interface ActionBundle<P = KnownPayloads._unionType> {
  type: ActionTypes,
  payload: P,
  errors?: Error[]
}



export interface MonoReducer<T extends keyof Store> {
  (state: Store[T], action: ActionBundle): false | Store[T]
}
export interface FlatReducer {
  (state: Store, action: ActionBundle): Store | false
}

export const initialState: Store = {
  waiting: false,
  addAreaDirty: addAreaDirtyState,
  employeeDirty: empDirtyState,
  addGroupDirty: groupDirtyState,
  applyLeaveDirty: leaveDirtyState,
  areas: [],
  employees: [],
  groups: [],
  leaveRequests: [],
  displayLog: []
}

const subReduce = <T extends keyof Store>(key: T, reducer: MonoReducer<T>): FlatReducer => {
  return (state, action) => {
    const result = reducer(state[key], action);
    if (!result) return false;
    return { ...state, [key]: result };
  }
}

const mainReducer = (state: Store = initialState, action: ActionBundle) => {
  addAreaDirtyReducer(addAreaDirtyState, action);
  return (
    apiResponseReducers(state, action) ||
    errorReducers(state, action) ||
    subReduce("addAreaDirty", addAreaDirtyReducer)(state, action) ||
    subReduce("employeeDirty", employeeDirtyReducer)(state, action) ||
    subReduce("addGroupDirty", groupDirtyReducer)(state, action) ||
    subReduce("applyLeaveDirty", leaveDirtyReducer)(state, action) ||

    state

  )
}

// if combinereducer is used later, add it here
export const rootReducer = mainReducer;
