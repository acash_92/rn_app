
declare module "project-defined-types/StoreDefinitions" {
    export namespace StoreKids {

        export enum UserType {
            ADMIN = "ADMIN",
            EMPLOYEE = "EMPLOYEE"
        }
        export interface User {
            email: string,
            name: string,
            profilePic: string,
            userType: UserType
        }
        export interface Area {
            name: string,
            levels: EscalationHandler[]
        }

        export interface EscalationHandler {
            groups: Group[],
            employees: Employee[],
            shouldComment: "yes" | "no" | "optional",
            timeout: number
        }

        export interface Employee {
            id: string,
            name: string,
            designation: string,
            email: string,
            picture?: string
        }
        export interface Group {
            id: string,
            name: string,
            members: Employee[]
        }

        export interface AddAreaDirtyState {
            areaName: string,
            escLevels: StoreKids.EscalationHandler[]
        }
    }

    export interface Store {
        profileReady: boolean;
        ready: boolean,
        user?: StoreKids.User,
        areas?: StoreKids.Area[],
        employees?: StoreKids.Employee[],
        groups?: StoreKids.Group[],
        addAreaDirty?: StoreKids.AddAreaDirtyState;
        currentErrors?: Error[],
    }
}