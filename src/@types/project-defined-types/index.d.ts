
declare module "project-defined-types" {
    import { IconDefinition } from "@fortawesome/free-solid-svg-icons";
    /// <reference path="StoreDefinitions.d.ts.d.ts" />
    export { Store } from "project-defined-types/StoreDefinitions";
    export { KnownPayloads } from "project-defined-types/ActionDefinitions";
    import { ComponentType } from "react";

    export type FilterFlags<Base, Condition> = {
        [Key in keyof Base]:
        Base[Key] extends Condition ? Key : never
    };

    export type SubType<Base, Condition> =
        Pick<Base, FilterFlags<Base, Condition>[keyof Base]>;

    export type AllowedKeys<Base, Condition> =
        keyof SubType<Base, Condition>

    export type PropDispatchMapping<Props, Bundle> = (dispatch: React.Dispatch<Bundle>) => SubType<Props, (_: any) => void>

    export type LooseObject<T = any> = {
        [key: string]: LooseObject<T> | Array<LooseObject<T>> | T
    } | T
    export namespace ThirdLayerAppbar {

        export type MenubarButtonDef<ActionTypes> = Array<{
            icon: IconDefinition,
            label: string,
            event: (dispatch: React.Dispatch<ActionBundle>) => void,
        }>
        export interface NavParams {
            headerRightButtonClick: () => void
        }
        export type ScreenDef<ScreenNames, ActionTypes> = {
            screenName: ScreenNames,
            screen: ComponentType<any>,
            menuBarButtons: MenubarButtonDef<ActionTypes>
        }
    }
    export interface S3SignedURL {
        signedRequestUrl: string,
        objectUrl: string
    }
    export interface SectionDescriptor<Item> {
        title: string,
        data: Item[]
    }

    export namespace GqlBuilder {

        export type Variables<T extends Object> = {
            [KEY in keyof T]: {
                type: keyof APITypes | string,
                required?: boolean,
                value: T[KEY]
            }
        }
    }

    // this should ideally be a namespace but i'm defining this as an interface so that I can access the keys as allowed types in GqlBuilder.variables
    export interface APITypes {
        ItemRange: { from: number, count?: number }
        DateRange: { from: string, to?: string }
    }
}


declare module 'project-defined-types/StoreDefinitions' {
    export namespace Store {

        export interface User {
            _id: string;
            name: string;
            designation: string;
            email: string;
            picture?: string;
            phone?: string;
            userType: "ADMIN" | "EMPLOYEE";
        }
        export interface Area {
            _id: string;
            name: string;
            areaCode: string;
            levels: EscalationHandler[];
        }
        export interface EscalationHandler {
            groups: Group[];
            // employees: Employee[];
            shouldComment: "yes" | "no" | "optional";
            timeout: number;
        }
        export type Employee = User
        export interface Group {
            _id: string;
            name: string;
            description?: string,
            members: Employee[];
        }

        export interface ErrorData<M extends Array<string | number> | never = Array<string | number>> extends Partial<Error> {
            message: string,
            mapToParams?: M
        }
        export type UIErrors = ErrorData[]

        export interface LeaveRequest {
            _id: string
            type: 'annual' | 'casual' | 'sick',
            startDate: Date,
            endDate: Date,
            days: number,
            status: 'approved' | 'pending' | 'rejected',
            comment?: string,
            timezone?: string,
            user?: Store.User | string
        }

        export interface Incident {
            area: Store.Area,
            status: "open" | "acknowledged" | "resolved",
            ackBy?: Store.User,
            triggerTime: Date
        }
    }
    export class Store {
        waiting: boolean;
        user?: Store.User;
        areas: Store.Area[];
        employees: Store.Employee[];
        groups: Store.Group[];
        leaveRequests: Store.LeaveRequest[];
        currentErrors?: Store.UIErrors;
        addAreaDirty: Store.Area & { shouldUpdate: boolean };
        employeeDirty: Store.Employee & { shouldUpdate: boolean };
        addGroupDirty: Store.Group & { shouldUpdate: boolean };
        applyLeaveDirty: Store.LeaveRequest & { shouldUpdate: boolean };
        displayLog: Store.Incident[]
    }
}


declare module 'project-defined-types/ActionDefinitions' {
    import { Store } from "project-defined-types/StoreDefinitions";
    import { APITypes } from "project-defined-types";

    export namespace KnownPayloads {
        export interface Login {
            email: string,
            password: string,
        }
        export type LoginSuccess = Pick<Store.User, "email" | "name" | "picture" | "userType">
        export type UIErrors = Store.UIErrors
        export interface Range<T> {
            from: T,
            to: T
        }
        export type LeaveDecision = { id: string, status: Store.LeaveRequest["status"] }
        export interface LeaveRequestParamas {
            status?: Store.LeaveRequest["status"],
            itemRange?: APITypes["ItemRange"],
            dateRange?: APITypes["DateRange"],
        }

        export interface UpdateCurrentUserPayload {
            name?: string,
            photo?: {
                userId: string,
                file: DocumentPickerResponse,
            },
            photoURL?: string,
            password?: string,
            auth?: string
        }

        export type AnyReducerPayloads = LoginSuccess
            | Store.Area | Store.Area[]
            | Store.Employee | Store.Employee[]
            | Store.Group | Store.Group[]
            | Store.LeaveRequest | Store.LeaveRequest[]
            | Store.Incident[]
            | UIErrors;
        export type AnySagaPayloads = Login
            | Range<Date>
            | LeaveDecision
            | LeaveRequestParamas
            | UpdateCurrentUserPayload
            | { id: string };

        export type _unionType = AnyReducerPayloads | AnySagaPayloads | undefined

    }

}