declare module 'react-native-selection-menu'{
    interface RNSelectionMenuType{
        Actionsheet:(props:any)=>void
        Alert:(props:any)=>void
        Formsheet:(props:any)=>void
    }
    export const RNSelectionMenu: RNSelectionMenuType
}