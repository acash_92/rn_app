declare module '@fortawesome/react-native-fontawesome' {
    import { ReactChild,Component } from 'react';
    import { IconDefinition } from '@fortawesome/free-solid-svg-icons'

    interface Props {
        icon: IconDefinition
        [key: string]: any
    }
    
    export class FontAwesomeIcon extends React.PureComponent<Props, any> { }
}