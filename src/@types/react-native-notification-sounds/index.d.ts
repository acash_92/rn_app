declare module 'react-native-notification-sounds' {

    export interface Sound {
        "soundID": string,
        "url": string,
        "title": string
    }

    interface PlaySampleSound {
        (sound: Sound): void
    }

    interface NotificationSounds {

        getNotifications: () => Promise<Sound[]>,
        playSampleSound: PlaySampleSound
    }
    const val: NotificationSounds
    export default val;
    export const playSampleSound: PlaySampleSound;

}