import { put, delay } from 'redux-saga/effects'
import { actionMaker } from '../Constants/ActionTypes'
import { Store, SubType } from "project-defined-types";
import { ActionBundle } from '../Redux/Reducers';
import { Query, Mutation } from "../Services/graphQLServices"

import { navigate } from '../Services/NavigationService'
import { ScreenNames } from '../Navigators/ScreenNames';
import { getOneEmployee, getOneArea, getOneGroup, getEmployees, getGroups } from '../Services/UserService'
/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */
export function* editArea({ payload }: ActionBundle<Store.Area>) {
  yield put(actionMaker.CLEAR_DIRTY_AREA());
  yield put(actionMaker.SET_DIRTY_ADD_AREA({ ...payload, shouldUpdate: true }));
  yield put(actionMaker.WAITING_RESPONSE());
  yield navigate(ScreenNames.AddAreaScreen, { title: "Edit " + payload.name })
  const qry = new Query();
  // getEmployees(qry);
  getGroups(qry);
  getOneArea(payload._id, qry);
  type ResponseShape = { /* employees: Store.Employee[], */ area: Store.Area, groups: Store.Group[] }
  const { /* employees, */ area, groups }: ResponseShape = yield qry.exec<ResponseShape>();
  yield put(actionMaker.SET_DIRTY_ADD_AREA({ ...area, shouldUpdate: true }));
  // yield put(actionMaker.SET_EMPLOYEE_LIST(employees));
  yield put(actionMaker.SET_GROUP_LIST(groups));
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());
}

export function* editEmployee({ payload }: ActionBundle<Store.Employee>) {
  yield put(actionMaker.CLEAR_DIRTY_EMPLOYEE());
  yield put(actionMaker.SET_DIRTY_EMPLOYEE({ ...payload, shouldUpdate: true }));
  yield put(actionMaker.WAITING_RESPONSE());
  yield navigate(ScreenNames.AddEmployeeScreen, { title: "Edit " + payload.name })
  const { employee: user } = yield getOneEmployee(payload._id).exec();
  yield put(actionMaker.SET_DIRTY_EMPLOYEE({ ...user, shouldUpdate: true }));
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());


}

export function* editGroup({ payload }: ActionBundle<Store.Group>) {
  yield put(actionMaker.CLEAR_DIRTY_GROUP());
  yield put(actionMaker.SET_DIRTY_GROUP({ ...payload, shouldUpdate: true }));
  yield put(actionMaker.WAITING_RESPONSE());
  yield navigate(ScreenNames.AddGroupScreen, { title: "Edit " + payload.name });
  const qry = new Query();
  getEmployees(qry);
  getOneGroup(payload._id, qry);

  type ResponseShape = { employees: Store.Employee[], group: Store.Group }
  const { employees, group }: ResponseShape = yield qry.exec<ResponseShape>();
  yield put(actionMaker.SET_DIRTY_GROUP({ ...group, shouldUpdate: true }));
  yield put(actionMaker.SET_EMPLOYEE_LIST(employees));
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());

}


export function* viewArea({ payload }: ActionBundle<Store.Area>) {
  yield put(actionMaker.CLEAR_DIRTY_AREA());
  yield put(actionMaker.SET_DIRTY_ADD_AREA({ ...payload, shouldUpdate: true }));
  yield put(actionMaker.WAITING_RESPONSE());
  yield navigate(ScreenNames.ViewAreaScreen, { title: payload.name })
  const qry = new Query();
  // getEmployees(qry);
  getGroups(qry);
  getOneArea(payload._id, qry);
  type ResponseShape = { /* employees: Store.Employee[], */ area: Store.Area, groups: Store.Group[] }
  const { /* employees,  */area, groups }: ResponseShape = yield qry.exec<ResponseShape>();
  yield put(actionMaker.SET_DIRTY_ADD_AREA({ ...area, shouldUpdate: true }));
  // yield put(actionMaker.SET_EMPLOYEE_LIST(employees));
  yield put(actionMaker.SET_GROUP_LIST(groups));
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());
}

export function* viewEmployee({ payload }: ActionBundle<Store.Employee>) {
  yield put(actionMaker.CLEAR_DIRTY_EMPLOYEE());
  yield put(actionMaker.SET_DIRTY_EMPLOYEE({ ...payload, shouldUpdate: true }));
  yield put(actionMaker.WAITING_RESPONSE());
  yield navigate(ScreenNames.ViewEmployeeScreen, { title: payload.name })
  const { employee: user } = yield getOneEmployee(payload._id).exec();
  yield put(actionMaker.SET_DIRTY_EMPLOYEE({ ...user, shouldUpdate: true }));
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());


}

export function* viewGroup({ payload }: ActionBundle<Store.Group>) {

  yield put(actionMaker.CLEAR_DIRTY_GROUP());
  yield put(actionMaker.SET_DIRTY_GROUP({ ...payload, shouldUpdate: true }));
  yield put(actionMaker.WAITING_RESPONSE());
  yield navigate(ScreenNames.ViewGroupScreen, { title: payload.name });
  const qry = new Query();
  getEmployees(qry);
  getOneGroup(payload._id, qry);

  type ResponseShape = { employees: Store.Employee[], group: Store.Group }
  const { employees, group }: ResponseShape = yield qry.exec<ResponseShape>();
  yield put(actionMaker.SET_DIRTY_GROUP({ ...group, shouldUpdate: true }));
  yield put(actionMaker.SET_EMPLOYEE_LIST(employees));
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());

}
