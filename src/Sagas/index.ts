import { takeLatest, all } from 'redux-saga/effects'
import { ActionTypes } from '../Constants/ActionTypes'
import { exitSplashscreen } from './Sagas'
import { loginSaga, logoutSaga, applyLeaveSaga, getMyLeaveReqsSaga, deleteLeaveReqSaga, updateUserSaga } from './userSaga'
import {
  saveAreaSaga, getMyAreas, getMyEmployees,
  getMyGroups, getMyLog, saveUserSaga, saveGroupSaga,
  deleteAreaSaga, deleteGroupSaga, deleteUserSaga,
  decideOnLeaveSaga
} from './adminSaga'
import { editArea, editEmployee, editGroup, viewArea, viewEmployee, viewGroup } from './navigationSAGA'

export default function* root() {
  yield all([
    /**
     * @see https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
     */
    // Run the startup saga when the application starts
    // takeLatest(StartupTypes.STARTUP, startup),
    // Call `fetchUser()` when a `FETCH_USER` action is triggered
    takeLatest(ActionTypes.APP_START_UP, exitSplashscreen),
    takeLatest(ActionTypes["api/login"], loginSaga),
    takeLatest(ActionTypes["api/logout"], logoutSaga),
    takeLatest(ActionTypes["api/saveArea"], saveAreaSaga),
    takeLatest(ActionTypes["api/updateCurrentUser"], updateUserSaga),
    takeLatest(ActionTypes["api/saveUser"], saveUserSaga),
    takeLatest(ActionTypes["api/saveGroup"], saveGroupSaga),
    takeLatest(ActionTypes["api/saveLeave"], applyLeaveSaga),
    takeLatest(ActionTypes["api/deleteLeave"], deleteLeaveReqSaga),
    takeLatest(ActionTypes["api/deleteArea"], deleteAreaSaga),
    takeLatest(ActionTypes["api/deleteGroup"], deleteGroupSaga),
    takeLatest(ActionTypes["api/deleteUser"], deleteUserSaga),
    takeLatest(ActionTypes.DECIDE_LEAVE, decideOnLeaveSaga),
    takeLatest(ActionTypes.EDIT_AREA, editArea),
    takeLatest(ActionTypes.EDIT_EMPLOYEE, editEmployee),
    takeLatest(ActionTypes.EDIT_GROUP, editGroup),
    takeLatest(ActionTypes.VIEW_AREA, viewArea),
    takeLatest(ActionTypes.VIEW_EMPLOYEE, viewEmployee),
    takeLatest(ActionTypes.VIEW_GROUP, viewGroup),
    takeLatest(ActionTypes.GET_EMPLOYEES, getMyEmployees),
    takeLatest(ActionTypes.GET_AREAS, getMyAreas),
    takeLatest(ActionTypes.GET_GROUPS, getMyGroups),
    takeLatest(ActionTypes.GET_LOG, getMyLog),
    takeLatest(ActionTypes.GET_LEAVE, getMyLeaveReqsSaga),


  ])
}
