
import { navigateAndReset } from '../Services/NavigationService'
import { ScreenNames } from '../Navigators/ScreenNames';
import { getToken, getCurrentUser } from '../Services/UserService'
import { actionMaker, ErrorKey } from '../Constants';
import { put, fork, cancel, race, join, call, delay } from 'redux-saga/effects'
import NetInfo from "@react-native-community/netinfo";
import { KnownPayloads } from 'project-defined-types';


export function* exitSplashscreen() {

  yield put(actionMaker.CLEAR_CURRENT_ERRORS())
  
  function* validateToken(token: string) {
    const query = getCurrentUser();
    const { employee: user } = yield query.exec();
    yield put(actionMaker.LOGIN_SUCCESS(user));
    navigateAndReset(user.userType === "ADMIN" ? ScreenNames.AdminScreenStack : ScreenNames.EmployeeScreenStack)
  }


  const state = yield NetInfo.fetch();
  if (!state.isConnected) {
    return put(actionMaker.SHOW_CURRENT_UI_ERROR([new Error(ErrorKey.NETWORK_ERROR)]));
  }

  const token = yield getToken();
  if (token) {
    try {
      // const validationTask = yield fork(validateToken, token);
      const { res, timeout } = yield race({
        res: call(validateToken, token),
        timeout: delay(3000)
      })

      if (timeout) {
        yield put(actionMaker.SHOW_CURRENT_UI_ERROR([new Error(ErrorKey.SERVER_NOT_RESPONDING)]));
        return;
      }
      return;
      // return navigateAndReset(ScreenNames.MainScreen);

      // return navigateAndReset([ScreenNames.MainScreen,user.userType==="ADMIN"? ScreenNames.Dashboard:ScreenNames.MyAcknowledgements])
    } catch (e) {
      console.log(e)
      yield put(actionMaker.SHOW_CURRENT_UI_ERROR([new Error(ErrorKey.SERVER_NOT_RESPONDING)]));
      return;

    }
  }


  return navigateAndReset(ScreenNames.LoginScreen);


}
