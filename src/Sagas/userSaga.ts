import { put } from 'redux-saga/effects'
import { actionMaker } from '../Constants/ActionTypes'
import { login, logout, getLeaveRequests, saveLeaveRequest, deleteLeaveReq, updateUser, uploadPhoto, getCurrentUser } from '../Services/UserService'
import { KnownPayloads } from "project-defined-types/ActionDefinitions";
import { ActionBundle } from '../Redux/Reducers';

import { navigateAndReset, goBack } from '../Services/NavigationService'
import { ScreenNames } from '../Navigators/ScreenNames';
import { store } from '../Redux/Stores';

/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */
export function* loginSaga({ payload }: ActionBundle<KnownPayloads.Login>) {

  if (!payload) {
    throw new Error("Payload can't be empty here");
  }
  yield put(actionMaker.WAITING_RESPONSE());

  try {
    const { email, password } = payload;
    yield login(email, password);
    yield put(actionMaker.APP_START_UP());
  } catch (error) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR([error]))
  }
  yield put(actionMaker.CLEAR_RESPONSE_WAIT());

}

export function* updateUserSaga({ payload }: ActionBundle<KnownPayloads.UpdateCurrentUserPayload>) {
  // const {name,password,photo,auth} = payload;
  try {
    yield put(actionMaker.WAITING_RESPONSE());
    if (payload.photo) {

      const s3ObjectUrl = yield uploadPhoto(payload.photo.file, payload.photo.userId)
      payload.photoURL = s3ObjectUrl + "?updatedOn=" + Date.now();
    }
    yield updateUser(payload);
    const query = getCurrentUser();
    const { employee: user } = yield query.exec();
    yield put(actionMaker.LOGIN_SUCCESS(user));
    yield put(actionMaker.CLEAR_RESPONSE_WAIT());
  } catch (e) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(e))
    yield put(actionMaker.CLEAR_RESPONSE_WAIT());
  }
}

export function* logoutSaga() {
  yield logout();
  yield put(actionMaker.CLEAR_SESSION())
  yield navigateAndReset(ScreenNames.LoginScreen);

}


export function* applyLeaveSaga() {
  const { shouldUpdate, ...payload } = store.getState().applyLeaveDirty;

  try {
    // const { levels, name } = payload;
    yield put(actionMaker.CLEAR_CURRENT_ERRORS())
    yield saveLeaveRequest(payload);
    yield put(actionMaker.CLEAR_DIRTY_AREA())
    yield put(actionMaker.GET_LEAVE({}));
    goBack();
  } catch (errors) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(errors))
  }
}


export function* getMyLeaveReqsSaga({ payload }: ActionBundle<KnownPayloads.LeaveRequestParamas>) {
  const { leaveRequests } = yield getLeaveRequests(payload).exec();
  yield put(actionMaker.SET_LEAVE_REQUEST_LIST(leaveRequests))
}


export function* deleteLeaveReqSaga({ payload: { id } }: ActionBundle<{ id: string }>) {
  yield deleteLeaveReq(id).exec();
  yield put(actionMaker.GET_LEAVE({}));
  return;
}