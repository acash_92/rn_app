import { put } from 'redux-saga/effects'
import { actionMaker, ActionTypes } from '../Constants/ActionTypes'
import { ActionBundle } from '../Redux/Reducers';
import { navigateAndReset, goBack } from '../Services/NavigationService'
import { KnownPayloads } from "project-defined-types/ActionDefinitions";
import {
  getEmployees, getGroups, getAreas, getLog, saveArea, saveGroup,
  saveUser, deleteArea, deleteUser, deleteGroup, saveLeaveRequest,
  getLeaveRequests, decideOnLeave
} from '../Services/UserService'

import { store } from "../Redux/Stores"
import { Alert } from 'react-native';
import { ErrorKey } from '../Constants';
/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */
export function* saveAreaSaga() {
  const payload = store.getState().addAreaDirty;
  try {
    // const { levels, name } = payload;
    yield put(actionMaker.CLEAR_CURRENT_ERRORS())
    yield saveArea(payload);
    // yield put(actionMaker.CLEAR_DIRTY_AREA());
    yield getMyAreas();
    goBack();
  } catch (errors) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(errors))
  }
}


export function* saveGroupSaga() {
  const { shouldUpdate, ...payload } = store.getState().addGroupDirty;

  try {
    // const { levels, name } = payload;
    yield put(actionMaker.CLEAR_CURRENT_ERRORS())
    yield saveGroup(payload);
    // yield put(actionMaker.CLEAR_DIRTY_AREA())
    yield getMyGroups();
    goBack();
  } catch (errors) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(errors))
  }
}

export function* saveUserSaga() {
  const { shouldUpdate, ...payload } = store.getState().employeeDirty;

  try {
    // const { levels, name } = payload;
    yield put(actionMaker.CLEAR_CURRENT_ERRORS())
    yield saveUser(payload);
    // yield put(actionMaker.CLEAR_DIRTY_EMPLOYEE())
    yield getMyEmployees();
    goBack();
  } catch (errors) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(errors))
  }
}


export function* getMyEmployees() {
  const { employees } = yield getEmployees().exec();
  yield put(actionMaker.SET_EMPLOYEE_LIST(employees))
}

export function* getMyGroups() {

  const { groups } = yield getGroups().exec();
  yield put(actionMaker.SET_GROUP_LIST(groups))
}

export function* getMyAreas() {
  const { areas } = yield getAreas().exec();
  yield put(actionMaker.SET_AREA_LIST(areas))
}

export function* getMyLog({ payload }: ActionBundle<KnownPayloads.Range<Date>>) {

}

export function* decideOnLeaveSaga({ payload: { id, status } }: ActionBundle<KnownPayloads.LeaveDecision>) {
  yield decideOnLeave(id, status).exec();
  const { leaveRequests } = yield getLeaveRequests({ status: "pending" }).exec();
  yield put(actionMaker.SET_LEAVE_REQUEST_LIST(leaveRequests))
}


function confirm(title: string, message: string) {
  return new Promise((resolve, reject) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: 'Cancel',
          onPress: () => resolve(false),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => resolve(true) },
      ],
      { cancelable: false },
    );
  })
}
export function* deleteAreaSaga({ payload: { id } }: ActionBundle<{ id: string }>) {
  try {
    const shouldDelete = yield confirm("Delete", "Are you sure you want to permanently delete this item");
    if (!shouldDelete) return;
    yield deleteArea(id).exec();
    goBack();
  } catch (e) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(e))
  }
}
export function* deleteUserSaga({ payload: { id } }: ActionBundle<{ id: string }>) {
  try {
    const shouldDelete = yield confirm("Delete", "Are you sure you want to permanently delete this item");
    if (!shouldDelete) return;
    yield deleteUser(id).exec();
    yield getMyEmployees();
    goBack();
  } catch (e) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(e))
  }
}
export function* deleteGroupSaga({ payload: { id } }: ActionBundle<{ id: string }>) {
  try {
    const shouldDelete = yield confirm("Delete", "Are you sure you want to permanently delete this item");
    if (!shouldDelete) return;
    yield deleteGroup(id).exec();
    yield getMyGroups();
    goBack();
  } catch (e) {
    yield put(actionMaker.SHOW_CURRENT_UI_ERROR(e))
  }
}