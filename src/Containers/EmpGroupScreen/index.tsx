import * as React from 'react'
import { Platform, Text, View, ListRenderItemInfo, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../Theme/ApplicationStyles'
import { FAButton } from '../../Components'
import { Store } from 'project-defined-types/StoreDefinitions'
import { FlatList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { navigate } from '../../Services/NavigationService'
import { ScreenNames } from '../../Navigators/ScreenNames'
import { Colors } from '../../Theme'
import { actionMaker } from '../../Constants/ActionTypes'
import { ActionBundle } from '../../Redux/Reducers'
import { DrawerContentComponentProps } from 'react-navigation-drawer/lib/typescript/src/types'

interface Props extends DrawerContentComponentProps {
  groups: Store.Group[],
  openGroupToView: (group: Store.Group) => void,
  loadGroups: () => void,
  launchNewGroupScreen: () => void
}


function Item({ data, onPress }: { data: Store.Group, onPress: (group: Store.Group) => void }) {
  return (
    <View style={ScreenStyle.listItem}>
      <TouchableOpacity onPress={() => onPress(data)}>
        <Text>{data.name}</Text>
      </TouchableOpacity>
    </View>
  );
}
const itemRenderer = (props: Props) => ({ item }: ListRenderItemInfo<Store.Group>) => {
  return <Item data={item} onPress={props.openGroupToView} />
}

class GroupList extends React.Component<Props, {}> {
  subscription: import("react-navigation").NavigationEventSubscription | undefined

  componentDidMount() {
    this.subscription = this.props.navigation.addListener("willFocus", () => {
      this.props.loadGroups();
    })
  }
  componentWillUnmount() {
    this.subscription?.remove();
  }

  render() {
    return (
      <View style={Style.container}>

        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
          <Text style={{ fontSize: 23 }}>Groups</Text>
          {/* <FontAwesomeIcon icon={faCalendarAlt} color="black" /> */}
        </View>
        {!this.props.groups.length && <Text style={ScreenStyle.heading}>Nothing to display</Text>}

        <FlatList
          data={this.props.groups}
          keyExtractor={(item) => item.name}
          renderItem={itemRenderer(this.props)}
        />

        <FAButton
          onPress={this.props.launchNewGroupScreen}
        >
          <FontAwesomeIcon icon={faPlus} color={Colors.primaryForeground} />
        </FAButton>
      </View >
    )
  }

  _fetchUser() {
    // this.props.fetchUser()
  }
}



const mapStateToProps = (state: Store) => ({
  groups: state.groups
  // [{
  //   id: "id",
  //   name: "Test Group with no escalation levels",
  //   members: [],
  // }]
})

// // tslint:disable-next-line: no-any
const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  openGroupToView: (group: Store.Group) => dispatch(actionMaker.VIEW_GROUP(group)),
  loadGroups: () => dispatch(actionMaker.GET_GROUPS()),
  launchNewGroupScreen: () => {
    dispatch(actionMaker.CLEAR_DIRTY_GROUP())
    navigate(ScreenNames.AddGroupScreen);
  }
})

export const GroupScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(GroupList)
