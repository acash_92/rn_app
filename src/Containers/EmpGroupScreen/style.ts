import { StyleSheet } from 'react-native'
import Fonts from '../../Theme/Fonts'
import {ScreenStyle} from '../../Theme/ApplicationStyles'

export const Style = StyleSheet.create({
  container: {
    ...ScreenStyle.container,
    margin: 30,
    flex: 1,
    justifyContent: 'center',
  },

})
