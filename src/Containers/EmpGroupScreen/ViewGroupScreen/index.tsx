import * as React from 'react'
import { Platform, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme/ApplicationStyles'
import { ScrollView } from 'react-navigation';
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { TextInput } from 'react-native-gesture-handler'
import { Button, MultiSelect, Card, InfoLine, ErrorLevels } from '../../../Components'
import { ErrorKey } from '../../../Constants/ErrorKeys'
import { ThirdLayerAppbar, Store } from 'project-defined-types'
import { ActionTypes, actionMaker } from '../../../Constants/ActionTypes'
import { ActionBundle } from '../../../Redux/Reducers'
import { store } from '../../../Redux/Stores'
import { ScreenNames } from '../../../Navigators/ScreenNames'

interface Props {
  currentErrors?: Store.UIErrors,
  employees: Store.Employee[],
  dirtyState: Store["addGroupDirty"],
  clearDirty: () => void,
  deleteGroup: (id: string) => void
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})



const ViewGroup: React.FC<Props> = (props) => {

  React.useEffect(() => {

    return () => {
      props.clearDirty();
    }
  }, []);

  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <View style={ScreenStyle.stack}>
            <Text>Full Name</Text>
            <Text>{props.dirtyState.name}</Text>
          </View>

          <View style={ScreenStyle.stack}>
            <Text>Description</Text>
            <Text>{props.dirtyState.description}</Text>


          </View>
          <View style={ScreenStyle.stack}>
            <MultiSelect<Store.Employee, "_id">
              data={props.employees}
              idKey={"_id"}
              labelKey={"name"}
              selection={props.dirtyState.members.map(employee => employee._id)}
              disabled={true}
              title={"Employees"}
              previewItemStyle={{ backgroundColor: "#0bf" }}
            />
          </View>

          <Card>
            <Text style={ScreenStyle.heading}>Remove user</Text>
            <Text>This action is non-revocable</Text>
            <InfoLine
              show={ErrorKey.GROUP_AREA_DEPENDENCY}
              level={ErrorLevels.CRITICAL}
              currentErrors={props.currentErrors} />
            <Button onPress={() => props.deleteGroup(props.dirtyState._id)}>Remove group</Button>
          </Card>
        </View>
      </ScrollView>
    </View >


  )

}



const mapStateToProps = (state: Store) => {

  return {
    employees: state.employees,
    currentErrors: state.currentErrors,
    dirtyState: state.addGroupDirty
  };
}


const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  clearDirty: () => dispatch(actionMaker.CLEAR_DIRTY_GROUP()),
  deleteGroup: (id: string) => dispatch(actionMaker["api/deleteGroup"]({ id })),
})



export const ViewGroupScreen = connect(mapStateToProps, mapDispatchToProps)(ViewGroup)


// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const ViewGroupScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.ViewGroupScreen,
  screen: ViewGroupScreen,
  menuBarButtons: [
    {
      icon: faEdit,
      label: "Save",
      event: (dispatch) => {
        const { addGroupDirty } = store.getState()
        dispatch(actionMaker.EDIT_GROUP(addGroupDirty))
      }
    }]
}
