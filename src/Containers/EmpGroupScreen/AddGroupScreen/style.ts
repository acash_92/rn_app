import { StyleSheet } from 'react-native'
import { Fonts, ScreenStyle } from '../../../Theme'

export const Style = StyleSheet.create({
  container: {
    ...ScreenStyle.container,
    margin: 30,
    flex: 1,
    justifyContent: 'center',
  },

})
