import * as React from 'react'
import { Platform, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme/ApplicationStyles'
import { Store } from 'project-defined-types/StoreDefinitions'
import { ScrollView } from 'react-navigation';
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import { TextInput } from 'react-native-gesture-handler'
import { InfoLine, ErrorLevels, MultiSelect } from '../../../Components'
import { ErrorKey } from '../../../Constants/ErrorKeys'
import { ThirdLayerAppbar } from 'project-defined-types'
import { ActionTypes, actionMaker } from '../../../Constants/ActionTypes'
import { ActionBundle } from '../../../Redux/Reducers'
import { ScreenNames } from '../../../Navigators/ScreenNames'

interface Props {
  currentErrors?: Store.UIErrors,
  employees: Store.Employee[],
  dirtyState: Store["addGroupDirty"],
  setDirty: (emp: Store.Group & { shouldUpdate: boolean }) => void,
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})



const AddGroup: React.FC<Props> = (props) => {
  const [_id, setId] = React.useState(props.dirtyState._id);
  const [name, setName] = React.useState(props.dirtyState.name);
  const [description, setDescription] = React.useState(props.dirtyState.description);
  const [members, setMembers] = React.useState(props.dirtyState.members);

  React.useEffect(() => {
    props.setDirty({ _id, name, members, description, shouldUpdate: false });
  }, [name, members, description]);

  React.useEffect(() => {
    if (!props.dirtyState.shouldUpdate) return;
    setName(props.dirtyState.name);
    setDescription(props.dirtyState.description);
    setMembers(props.dirtyState.members);
  }, [props.dirtyState.shouldUpdate])
  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <View style={ScreenStyle.stack}>
            <Text>Full Name</Text>
            <TextInput placeholder="Full Name" value={name} onChangeText={setName} />
            <InfoLine show={ErrorKey.NAME_REQUIRED} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} />

          </View>

          <View style={ScreenStyle.stack}>
            <Text>Description</Text>
            <TextInput
              placeholder="Description"
              value={description}
              multiline={true}
              numberOfLines={4}
              onChangeText={setDescription} />

          </View>
          <View style={ScreenStyle.stack}>
            <Text>Members</Text>
            <MultiSelect<Store.Employee, "_id">
              data={props.employees}
              idKey={"_id"}
              labelKey={"name"}
              selection={members.map(employee => employee._id)}
              onChange={setMembers}
              title={"Employees"}
              secondLineKey={"designation"}
              previewItemStyle={{ backgroundColor: "#0bf" }}
            />
            <InfoLine
              show={ErrorKey.EMPLOYEE_REQUIRED}
              level={ErrorLevels.CRITICAL}
              watchValue={members.reduce((str, cur) => (str + cur.name + ':'), "")}
              currentErrors={props.currentErrors} />
          </View>


        </View>
      </ScrollView>
    </View >


  )

}



const mapStateToProps = (state: Store) => {


  // const employees: Store.Employee[] = (new Array(20)).fill(0).map((val, index) => ({
  //   _id: "id" + index,
  //   name: "Test Employee",
  //   designation: "string",
  //   email: "string",
  //   picture: "string",
  //   userType: "EMPLOYEE",
  // }));

  return {
    employees: state.employees,
    currentErrors: state.currentErrors,
    dirtyState: state.addGroupDirty
  };
}


const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  setDirty: (emp: Store.Group & { shouldUpdate: boolean }) => dispatch(actionMaker.SET_DIRTY_GROUP(emp))
})



export const AddGroupScreen = connect(mapStateToProps, mapDispatchToProps)(AddGroup)


// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const AddGroupScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.AddGroupScreen,
  screen: AddGroupScreen,
  menuBarButtons: [{
    icon: faCheck,
    label: "Save",
    event: (dispatch)=>dispatch(actionMaker["api/saveGroup"]())
  }]
}