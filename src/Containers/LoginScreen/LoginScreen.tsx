import * as React from 'react'
import { Platform, Text, View, ActivityIndicator, Image, TextInput } from 'react-native'
import { Button, InfoLine, ErrorLevels } from "../../Components"
import { Style } from './style'
import { Images } from '../../Theme'
import { Store, KnownPayloads } from 'project-defined-types'
import { actionMaker } from "../../Constants/ActionTypes";
import { ErrorKey } from '../../Constants/ErrorKeys'
import { connect } from 'react-redux'
import { NavigationScreenProp, NavigationState, NavigationParams } from 'react-navigation'
import { ActionBundle } from "../../Redux/Reducers";


// https://www.npmjs.com/package/react-native-notification-sounds
// ios is not configured. folllow instructions there to do it
const platformMessages = Platform.select({
  ios: () => {
    throw new Error(`
 https://www.npmjs.com/package/react-native-notification-sounds
 ios is not configured. folllow instructions there to do it
`)
  },
  android: () => { },
})();

interface Props {
  currentErrors: KnownPayloads.UIErrors | undefined,
  login: (email: string, password: string) => void,
  navigation: NavigationScreenProp<NavigationState, NavigationParams>,
  waiting: boolean
}

const LoginScreenComponent: React.FC<Props> = ({ currentErrors, navigation, waiting, ...props }) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const { message }: { message?: string } = navigation.state.params || {};
  const login = () => {
    props.login(email, password);
  }
  return (
    <View style={Style.container}>
      <View>
        <View style={Style.logoContainer}>
          <Image style={Style.logo} source={Images.logo} resizeMode={'contain'} />
        </View>
        <Text style={Style.text}>{message || "Login"}</Text>
        <TextInput onChangeText={text => setEmail(text)} value={email} placeholder="username" />
        <TextInput onChangeText={text => setPassword(text)} value={password} secureTextEntry={true} placeholder="password" />
        <InfoLine show={ErrorKey.CREDENTIAL_MISMATCH} level={ErrorLevels.CRITICAL} currentErrors={currentErrors} />
        <InfoLine show={ErrorKey.NETWORK_ERROR} level={ErrorLevels.WARNING} currentErrors={currentErrors} />
        <Button onPress={login} waiting={waiting}><Text>Login</Text></Button>
      </View>
    </View>
  )
}

const mapStateToProps = (state: Store) => ({
  currentErrors: state.currentErrors,
  waiting: state.waiting
})
const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  login: (email: string, password: string) => dispatch(actionMaker["api/login"]({ email, password }))
})
export const LoginScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreenComponent)
