import * as React from 'react'
import { Platform, Text, View, TouchableOpacity, Picker, Alert } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme/ApplicationStyles'
import { ScrollView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faTrashAlt, faCheck } from '@fortawesome/free-solid-svg-icons'
import { TextInput } from 'react-native-gesture-handler'
import { MultiSelect, InfoLine, ErrorLevels, GeneralErrorLines, Button, Card } from '../../../Components'
import { ThirdLayerAppbar, Store, KnownPayloads } from 'project-defined-types';
import { ActionBundle } from '../../../Redux/Reducers'
import { actionMaker, ActionTypes } from '../../../Constants/ActionTypes'
import { ErrorKey } from '../../../Constants/ErrorKeys'
import { ScreenNames } from '../../../Navigators/ScreenNames'
import { DrawerContentComponentProps } from 'react-navigation-drawer'



interface Props extends DrawerContentComponentProps {

  currentErrors: KnownPayloads.UIErrors | undefined,
  user?: Store.User,
  groups: Store.Group[],
  // employees: Store.Employee[]
  dirtyState: Store["addAreaDirty"],
  setDirty: (area: Store["addAreaDirty"]) => void
}

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})






const AddArea: React.FC<Props> = (props) => {

  const emptyEscLevel: Store.EscalationHandler = {
    groups: [],
    // employees: [],
    shouldComment: "no",
    timeout: 5
  }

  // const [dirtyStateLoaded, setDirtyStateFlag] = React.useState(props.dirtyState._id ? false : true);
  const [areaName, setAreaName] = React.useState('');
  const [areaCode, setAreaCode] = React.useState('');
  const [_id, setId] = React.useState('');
  const [escLevels, updateEscLevels] = React.useState<Store.EscalationHandler[]>([]);

  React.useEffect(() => {
    props.setDirty({ _id, name: areaName, levels: escLevels, shouldUpdate: false, areaCode });
  }, [areaName, areaCode, escLevels]);

  React.useEffect(() => {
    if (!props.dirtyState.shouldUpdate) return;
    setAreaName(props.dirtyState.name);
    setAreaCode(props.dirtyState.areaCode);
    updateEscLevels(props.dirtyState.levels);
    setId(props.dirtyState._id);
  }, [props.dirtyState.shouldUpdate]);

  function addEscLevel() {
    updateEscLevels([
      ...escLevels,
      { ...emptyEscLevel }
    ])
  }
  function replaceEscLevel(position: number, escHandler: Store.EscalationHandler) {
    const newConfig = escLevels.map((val, index) => {
      if (index === position) return escHandler;
      return val
    })
    updateEscLevels(newConfig);
  }

  function removeEscLevel(position: number) {
    Alert.alert(
      "Remove escalation level",
      "Are you sure you want to remove this level",
      [
        {
          text: 'Cancel',
          onPress: () => {/*do nothing*/ },
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => {

            const newConfig = escLevels.filter((val, index) => {
              return index !== position
            })
            updateEscLevels(newConfig);
          }
        },
      ],
      { cancelable: false },
    );

  }

  function updateEscValues<K extends keyof Store.EscalationHandler>(position: number, key: K, val: Store.EscalationHandler[K]) {
    const newEsc = {
      ...escLevels[position],
      [key]: val
    }
    replaceEscLevel(position, newEsc)
  }


  // let errorMap: LooseObject = {};
  // if (props.currentErrors) {
  //   errorMap = structureMappedErrors(props.currentErrors)
  // }
  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <Text>Area Name</Text>

          <TextInput placeholder="Area name" value={areaName} onChangeText={setAreaName} />
          <InfoLine
            show={ErrorKey.NAME_REQUIRED}
            level={ErrorLevels.CRITICAL}
            watchValue={areaName}
            currentErrors={props.currentErrors} />

          <Text>Area Code</Text>
          <TextInput placeholder="Area code" value={areaCode} onChangeText={setAreaCode} />
          <InfoLine
            show={ErrorKey.DUPLICATE_AREA_CODE}
            level={ErrorLevels.CRITICAL}
            watchValue={areaCode}
            currentErrors={props.currentErrors} />
          <InfoLine
            show={ErrorKey.AREA_CODE_REQUIRED}
            level={ErrorLevels.CRITICAL}
            watchValue={areaCode}
            currentErrors={props.currentErrors} />
          {
            escLevels.map((esc, index) => (
              <Card key={index}>
                <View style={[ScreenStyle.row, { justifyContent: "space-between" }]}>
                  <Text>Level {index + 1}</Text>
                  <TouchableOpacity onPress={() => removeEscLevel(index)}>
                    <FontAwesomeIcon icon={faTrashAlt} />
                  </TouchableOpacity>
                </View>
                <Text>Employee Groups</Text>
                <MultiSelect<Store.Group, "_id">
                  data={props.groups}
                  idKey={"_id"}
                  labelKey={"name"}
                  selection={esc.groups.map(group => group._id)}
                  onChange={(vals) => updateEscValues(index, "groups", vals)}
                  title={"Groups"}
                />
                {/* 
                <Text>Employees</Text>
                <MultiSelect<Store.Employee, "_id">
                  data={props.employees}
                  idKey={"_id"}
                  labelKey={"name"}
                  selection={esc.employees.map(employee => employee._id)}
                  onChange={(vals) => updateEscValues(index, "employees", vals)}
                  title={"Employees"}
                /> */}
                {/* <InfoLine
                  show={ErrorKey.ESC_LEVEL_EMPS_OR_GROUPS_REQUIRED}
                  level={ErrorLevels.CRITICAL}
                  watchValue={esc.groups.length + esc.employees.length}
                  matchPattern={["levels", index]}
                  currentErrors={props.currentErrors} /> */}

                <Text>Comments </Text>
                <Picker selectedValue={esc.shouldComment} onValueChange={val => updateEscValues(index, "shouldComment", val)}>
                  <Picker.Item label="yes" value="yes" />
                  <Picker.Item label="no" value="no" />
                  <Picker.Item label="optional" value="optional" />
                </Picker>
              </Card>
            ))
          }
          {/*           {escLevels.length ? null : (
            <Text>Add atleast on escalation level to continue</Text>
          )} */}

          <Button onPress={addEscLevel} >Add escalation level</Button>
          <InfoLine
            watchValue={escLevels.length}
            show={ErrorKey.AREA_ESC_LEVEL_REQUIRED}
            level={ErrorLevels.CRITICAL}
            currentErrors={props.currentErrors} />
          <GeneralErrorLines currentErrors={props.currentErrors} />
        </View>
      </ScrollView>
    </View >


  )


}



const mapStateToProps = (state: Store) => {
  const {/*  employees, */ groups, currentErrors, addAreaDirty: dirtyState } = state;
  return {/*  employees, */ groups, currentErrors, dirtyState };
}


const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  setDirty: (area: Store["addAreaDirty"]) => dispatch(actionMaker.SET_DIRTY_ADD_AREA(area))
})

const AddAreaScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddArea)

// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const AddAreaScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.AddAreaScreen,
  screen: AddAreaScreen,
  menuBarButtons: [
    {
      icon: faCheck,
      label: "Save",
      event: (dispatch) => dispatch(actionMaker["api/saveArea"]())
    }]
}