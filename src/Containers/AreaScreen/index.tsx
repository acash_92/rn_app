import * as React from 'react'
import { Platform, Text, View, Button, ActivityIndicator, Image, SectionListData, SectionListRenderItemInfo, ListRenderItemInfo, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../Theme/ApplicationStyles'
import { Store } from 'project-defined-types/StoreDefinitions'
import { FlatList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { navigate } from '../../Services/NavigationService'
import { ScreenNames } from '../../Navigators/ScreenNames'
import { FAButton } from "../../Components"
import { Colors } from '../../Theme'
import { actionMaker } from '../../Constants/ActionTypes'
import { ActionBundle } from '../../Redux/Reducers'
import { DrawerContentComponentProps } from 'react-navigation-drawer/lib/typescript/src/types'

interface Props extends DrawerContentComponentProps {
  areas: Store.Area[],
  openAreaToView: (area: Store.Area) => void,
  getAreas: () => void,
  launchNewAreaScreen: () => void
}


const itemRenderer = (itemPressHandler: Props["openAreaToView"]) => ({ item }: ListRenderItemInfo<Store.Area>) => {
  return <View style={ScreenStyle.listItem}>
    <TouchableOpacity onPress={() => itemPressHandler(item)}>
      <Text>{item.name}</Text>
    </TouchableOpacity>
  </View>
}

class AreaList extends React.Component<Props, {}> {
  subscription: import("react-navigation").NavigationEventSubscription | undefined

  componentDidMount() {
    this.subscription = this.props.navigation.addListener("willFocus", () => {
      this.props.getAreas();
    })
  }

  componentWillUnmount() {
    this.subscription?.remove();
  }

  render() {
    return (
      <View style={Style.container}>

        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
          <Text style={{ fontSize: 23 }}>Areas</Text>
          {/* <FontAwesomeIcon icon={faCalendarAlt} color="black" /> */}
        </View>
        {!this.props.areas.length && <Text style={ScreenStyle.heading}>Nothing to display</Text>}

        <FlatList
          data={this.props.areas}
          keyExtractor={(item) => item.name}
          renderItem={itemRenderer(this.props.openAreaToView)}
        />
        <FAButton
          onPress={this.props.launchNewAreaScreen}
        >
          <FontAwesomeIcon icon={faPlus} color={Colors.primaryForeground} />
        </FAButton>
      </View >
    )
  }
}



const mapStateToProps = (state: Store) => ({
  areas: state.areas
})

// // tslint:disable-next-line: no-any
const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  openAreaToView: (area: Store.Area) => dispatch(actionMaker.VIEW_AREA(area)),
  getAreas: () => dispatch(actionMaker.GET_AREAS()),
  launchNewAreaScreen: () => {
    dispatch(actionMaker.CLEAR_DIRTY_AREA())
    navigate(ScreenNames.AddAreaScreen);
  }
})

export const AreaScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(AreaList)
