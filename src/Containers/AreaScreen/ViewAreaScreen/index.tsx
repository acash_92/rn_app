import * as React from 'react'
import { Platform, Text, View, TouchableOpacity, Picker } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme/ApplicationStyles'
import { ScrollView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { TextInput } from 'react-native-gesture-handler'
import { MultiSelect, Button, Card } from '../../../Components'
import { ThirdLayerAppbar, Store, KnownPayloads } from 'project-defined-types';
import { ActionBundle } from '../../../Redux/Reducers'
import { actionMaker, ActionTypes } from '../../../Constants/ActionTypes'
import { ErrorKey } from '../../../Constants/ErrorKeys'
import { ScreenNames } from '../../../Navigators/ScreenNames'
import { store } from '../../../Redux/Stores'
import { DrawerContentComponentProps } from 'react-navigation-drawer'



interface Props extends DrawerContentComponentProps {

  user?: Store.User,
  groups: Store.Group[],
  // employees: Store.Employee[]
  dirtyState: Store["addAreaDirty"],
  clearDirty: () => void,
  deleteArea: (id: string) => void
}


const ViewArea: React.FC<Props> = (props) => {


  React.useEffect(() => {

    return () => {
      props.clearDirty();
    }
  }, []);



  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <Text>Area Name</Text>

          <Text style={ScreenStyle.heading}>{props.dirtyState.name}</Text>


          <Text>Area Code</Text>
          <Text style={ScreenStyle.heading} >{props.dirtyState.areaCode}</Text>

          {
            props.dirtyState.levels.map((esc, index) => (
              <Card key={index}>
                <View style={[ScreenStyle.row, { justifyContent: "space-between" }]}>
                  <Text style={ScreenStyle.heading}>Level {index + 1}</Text>
                </View>
                <Text>Employee Groups</Text>
                <MultiSelect<Store.Group, "_id">
                  data={props.groups}
                  idKey={"_id"}
                  labelKey={"name"}
                  selection={esc.groups.map(group => group._id)}
                  title={"Groups"}
                  disabled={true}
                />

                {/* <Text>Employees</Text>
                <MultiSelect<Store.Employee, "_id">
                  data={props.employees}
                  idKey={"_id"}
                  labelKey={"name"}
                  selection={esc.employees.map(employee => employee._id)}
                  disabled={true}
                  title={"Employees"}
                />
 */}

                <Text>Comments: {esc.shouldComment ? "Yes" : "No"} </Text>
              </Card>
            ))
          }
          <Card>
            <Text style={ScreenStyle.heading}>Remove area</Text>
            <Text>This action is non-revocable</Text>
            <Button onPress={() => props.deleteArea(props.dirtyState._id)}>Remove area</Button>
          </Card>
        </View>
      </ScrollView>
    </View >


  )


}



const mapStateToProps = (state: Store) => {
  const { /* employees, */ groups, currentErrors, addAreaDirty: dirtyState } = state;
  return { /* employees, */ groups, currentErrors, dirtyState };
}


const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  clearDirty: () => dispatch(actionMaker.CLEAR_DIRTY_AREA()),
  deleteArea: (id: string) => dispatch(actionMaker["api/deleteArea"]({ id })),
})

export const ViewAreaScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewArea)

// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const ViewAreaScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.ViewAreaScreen,
  screen: ViewAreaScreen,
  menuBarButtons: [
    {
      icon: faEdit,
      label: "Save",
      event: (dispatch) => {
        const { addAreaDirty } = store.getState()
        dispatch(actionMaker.EDIT_AREA(addAreaDirty))
      }
    }]
}
