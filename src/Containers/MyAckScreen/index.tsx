import * as React from 'react'
import { Platform, Text, View, Button, ActivityIndicator, Image } from 'react-native'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import Style from './DashboardScreenStyle'
import { Images } from '../../Theme'
import { StoreKids, Store } from '../../Redux/StoreModel'
import { ActionTypes } from "../../Constants/ActionTypes";
import { ActionBundle } from '../../Redux/Reducers'


interface Props {
  user?: StoreKids.User,
  userIsLoading?: boolean,
  userErrorMessage?: string,
  fetchUser?: () => void,
  liveInEurope?: boolean,
}

/**
 * This is an example of a container component.
 *
 * This screen displays a little help message and informations about a fake user.
 * Feel free to remove it.
 */

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})


class ExampleScreen extends React.Component<Props, {}> {


  componentDidMount() {
    this._fetchUser()
  }

  render() {
    return (
      <View style={Style.container}>
        {this.props.userIsLoading ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
            <View>
              <View style={Style.logoContainer}>
                <Image style={Style.logo} source={Images.logo} resizeMode={'contain'} />
              </View>
              <Text style={Style.text}>To get started, edit App.js</Text>
              <Text style={Style.instructions}>{instructions}</Text>
              {this.props.userErrorMessage ? (
                <Text style={Style.error}>{this.props.userErrorMessage}</Text>
              ) : (
                  <View>
                    <Text style={Style.result}>
                      {"I'm a fake user, my name is "}
                      {this.props.user && this.props.user.name}
                    </Text>
                    <Text style={Style.result}>
                      {this.props.liveInEurope ? 'I live in Europe !' : "I don't live in Europe."}
                    </Text>
                  </View>
                )}
              <Button onPress={() => this._fetchUser()} title="Refresh" />
            </View>
          )}
      </View>
    )
  }

  _fetchUser() {
    // this.props.fetchUser()
  }
}



const mapStateToProps = (state: Store) => ({
  user: state.user,
  userIsLoading: state.ready,
  userErrorMessage: "",
  liveInEurope: false,
})

// // tslint:disable-next-line: no-any
// const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => {
//   // no actions defined
//   dispatch({ type: ActionTypes["api/login"], payload: { username: 'eee', name: "ssss" } });
// }

export default connect(
  mapStateToProps
)(ExampleScreen)
