import * as React from 'react'
import { Platform, Text, View, Button, ActivityIndicator, Image, SectionListData, SectionListRenderItemInfo } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { EventItem } from '../../Components'
import { Store } from 'project-defined-types/StoreDefinitions'
import { SectionList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'

interface Props {
  user?: Store.User,
  userIsLoading?: boolean,
  userErrorMessage?: string,
  fetchUser?: () => void,
  liveInEurope?: boolean,
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})

type DataItem = string;
export interface DataSection extends SectionListData<DataItem> {
  title: string
}
const DATA: DataSection[] = [
  {
    title: 'Main dishes',
    data: ['Pizza', 'Burger', 'Risotto'],
  },
  {
    title: 'Sides',
    data: ['French Fries', 'Onion Rings', 'Fried Shrimps'],
  },
  {
    title: 'Drinks',
    data: ['Water', 'Coke', 'Beer'],
  },
  {
    title: 'Desserts',
    data: ['Cheese Cake', 'Ice Cream'],
  },
];


function Item({ data }: { data: string }) {
  return (
    <View style={{
      backgroundColor: '#009',
      padding: 20,
      marginVertical: 8,
    }}>
      <Text style={{ fontSize: 24 }}>{data}nnn</Text>
    </View>
  );
}
const itemRenderer = ({ item }: SectionListRenderItemInfo<string>) => {
  return <EventItem data={item} />
}

const sectionRenderer = ({ section }: { section: SectionListData<string> }) => (
  <Text style={{ fontSize: 15, padding: 5 }}>Today 20 February 2019 - {section.title}</Text>
)

class AlarmHistory extends React.Component<Props, {}> {


  componentDidMount() {
    this._fetchUser()
  }


  render() {
    return (
      <View style={Style.container}>

        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
          <Text style={{ fontSize: 23 }}>All alarms</Text>
          <FontAwesomeIcon icon={faCalendarAlt} color="black" />
        </View>
        <SectionList<DataItem>
          sections={DATA}
          keyExtractor={(item, index) => item + index}
          renderItem={itemRenderer}
          renderSectionHeader={sectionRenderer}
        />
        {/* )} */}
      </View >
    )
  }

  _fetchUser() {
    // this.props.fetchUser()
  }
}



const mapStateToProps = (state: Store) => ({
  user: state.user,
  userErrorMessage: "",
  liveInEurope: false,
})

// // tslint:disable-next-line: no-any
// const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => {
//   // no actions defined
//   dispatch({ type: ActionTypes["api/login"], payload: { username: 'eee', name: "ssss" } });
// }

export const AlarmHistoryScreen = connect(
  mapStateToProps
)(AlarmHistory)
