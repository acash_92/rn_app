import * as React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import styles from './SplashScreenStyle'
import { Store } from 'project-defined-types'
import { Dispatch } from 'redux'
import { actionMaker } from '../../Constants/ActionTypes'
import { connect } from 'react-redux'
import { ActionBundle } from "../../Redux/Reducers";
import { InfoLine, ErrorLevels } from '../../Components'
import { ErrorKey } from '../../Constants'

interface Props {
  startup: () => void,
  currentErrors?: Store.UIErrors
}

export class SplashScreenComponent extends React.Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          {/* You will probably want to insert your logo here */}
          <Text>LOGO</Text>

        </View>
        <InfoLine level={ErrorLevels.WARNING} show={ErrorKey.SERVER_NOT_RESPONDING} currentErrors={this.props.currentErrors}>
          <TouchableOpacity onPress={this.props.startup}>
            <Text>Try again</Text>
          </TouchableOpacity>
        </InfoLine>
      </View>
    )
  }
}

const mapStateToProps = (state: Store) => ({
  currentErrors: state.currentErrors
})

const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => ({
  startup: () => dispatch(actionMaker.APP_START_UP())

})

// tslint:disable-next-line: variable-name
export const SplashScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashScreenComponent)
