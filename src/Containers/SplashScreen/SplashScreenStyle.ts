import { StyleSheet } from 'react-native'
import Colors from '../../Theme/Colors'
import {ScreenStyle} from '../../Theme/ApplicationStyles'

// tslint:disable-next-line: no-default-export
export default StyleSheet.create({
  container: {
    ...ScreenStyle.container,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primaryBackground,
  },
  logo: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    backgroundColor: 'white',
  },
})
