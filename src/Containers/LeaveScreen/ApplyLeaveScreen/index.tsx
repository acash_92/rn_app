import * as React from 'react'
import { Platform, Text, View, TouchableOpacity, Picker } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme'
import { Store } from 'project-defined-types/StoreDefinitions'
import { ScrollView } from 'react-navigation';
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import { TextInput, State } from 'react-native-gesture-handler'
import { ThirdLayerAppbar } from 'project-defined-types'
import { actionMaker } from '../../../Constants/ActionTypes'
import { ActionBundle } from '../../../Redux/Reducers'
import DateTimePicker from '@react-native-community/datetimepicker'
import { ScreenNames } from '../../../Navigators/ScreenNames'

interface Props {
  currentErrors?: Store.UIErrors,
  dirtyState: Store["applyLeaveDirty"],
  user?: Store.User,
  setDirty: (leaveReq: Store["applyLeaveDirty"]) => void,
  clearDirty: () => void
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})



const ApplyLeave: React.FC<Props> = (props) => {

  function tomorrowIfNot(date: Date | undefined) {
    const today = new Date();
    const tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1)
    return date ? new Date(date) : tomorrow;
  }

  function pickingDone(setter: (val: boolean) => void) {
    setter(Platform.OS === 'ios' ? true : false);
    return true;
  }

  function dateString(date: Date) {
    return date.toLocaleDateString()
  }

  function applyStartDate(date: Date) {
    setStartDate(date);
    if (endDate < date) setEndDate(date);
  }


  const _id = props.dirtyState._id || "";
  const [type, setType] = React.useState(props.dirtyState.type || "annual");
  const [startDate, setStartDate] = React.useState<Date>(tomorrowIfNot(undefined));
  const [endDate, setEndDate] = React.useState<Date>(tomorrowIfNot(undefined));
  const [comment, setComment] = React.useState(props.dirtyState.comment);
  const [showStartDatePicker, setShowStartDatePicker] = React.useState(false);
  const [showEndDatePicker, setShowEndDatePicker] = React.useState(false);

  React.useEffect(() => {
    props.setDirty({ _id, type, startDate, endDate, comment, days: 0, status: "pending", shouldUpdate: false, user: props.user && props.user._id });
    return props.clearDirty;
  }, [type, startDate, endDate, comment]);


  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <View style={ScreenStyle.stack}>
            <Text>Leave type</Text>
            <Picker selectedValue={type} onValueChange={setType}>
              <Picker.Item label="Annual" value="annual" />
              <Picker.Item label="Casual" value="casual" />
              <Picker.Item label="Sick" value="sick" />
            </Picker>

          </View>

          <View style={ScreenStyle.stack}>
            <TouchableOpacity onPress={() => setShowStartDatePicker(!showStartDatePicker)}>
              <Text>Start Date</Text>
              <TextInput placeholder="Start Date" value={dateString(startDate)} editable={false} />
            </TouchableOpacity>

            {showStartDatePicker && <DateTimePicker value={startDate}
              mode="date"
              is24Hour={true}
              display="default"
              minimumDate={tomorrowIfNot(undefined)}
              onChange={(event, date) => pickingDone(setShowStartDatePicker) && applyStartDate(tomorrowIfNot(date))} />
            }

            <TouchableOpacity onPress={() => setShowEndDatePicker(!showEndDatePicker)}>
              <Text>End Date</Text>
              <TextInput placeholder="Start Date" value={dateString(endDate)} editable={false} />
            </TouchableOpacity>

            {showEndDatePicker && <DateTimePicker value={endDate}
              mode="date"
              is24Hour={true}
              display="default"
              minimumDate={tomorrowIfNot(startDate)}
              onChange={(event, date) => pickingDone(setShowEndDatePicker) && setEndDate(tomorrowIfNot(date))} />
            }
            {/* <ErrorLine show={ErrorKey.EMP_NAME_REQUIRED} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} /> */}


            <View style={ScreenStyle.stack}>
              <Text>Comments</Text>
              <TextInput
                placeholder="Comments"
                value={comment}
                multiline={true}
                numberOfLines={4}
                onChangeText={setComment} />

            </View>
          </View>

        </View>
      </ScrollView>
    </View >


  )

}



const mapStateToProps = (state: Store) => ({
  // area: state.areas,

  currentErrors: state.currentErrors,
  dirtyState: state.applyLeaveDirty,
  user: state.user
})

const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  clearDirty: () => dispatch(actionMaker.CLEAR_DIRTY_LEAVE_REQUEST()),
  setDirty: (leaveReq: Store["applyLeaveDirty"]) => dispatch(actionMaker.SET_DIRTY_LEAVE_REQUEST(leaveReq))
})



export const ApplyLeaveScreen = connect(mapStateToProps, mapDispatchToProps)(ApplyLeave)

// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const ApplyLeaveScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.ApplyLeaveScreen,
  screen: ApplyLeaveScreen,
  menuBarButtons: [{
    icon: faCheck,
    label: "Save",
    event: (dispatch)=>dispatch(actionMaker["api/saveLeave"]())
  }]
}