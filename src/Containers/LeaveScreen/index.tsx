import * as React from 'react'
import { Platform, Text, View, TouchableOpacity, ListRenderItem, ListRenderItemInfo } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { EventItem, FAButton } from '../../Components'
import { Store } from 'project-defined-types'
import { FlatList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faTrashAlt, faExclamationCircle, faCheckCircle, faClock } from '@fortawesome/free-solid-svg-icons'
import { navigate } from '../../Services/NavigationService'
import { ScreenNames } from '../../Navigators/ScreenNames'
import { Colors, ScreenStyle } from '../../Theme'
import { ActionTypes, actionMaker } from '../../Constants'
import { ActionBundle } from '../../Redux/Reducers'
import { Dispatch } from 'redux'

interface Props {
  leaveRequestList: Store.LeaveRequest[]
  deleteItem: (id: string) => void,
  loadLeaveRequests: () => void
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})


const itemRenderer = (deleteHandler: Props["deleteItem"]) => ({ item: leaveItem }: ListRenderItemInfo<Store.LeaveRequest>) => {

  return <View style={Style.row}>
    <View style={Style.timeBlock}>
      <Text style={{ fontSize: 18 }}>{leaveItem.days}{leaveItem.days > 1 ? " days" : " day"}</Text>
    </View>
    <View style={Style.areaBlock}>
      <Text style={{ fontSize: 18 }}>
        {leaveItem.startDate.toLocaleDateString()}
        {leaveItem.days > 1 ? " - " + leaveItem.endDate.toLocaleDateString() : ""}
      </Text>
      <Text> {leaveItem.type}</Text>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        {leaveItem.status === "approved" ? <FontAwesomeIcon icon={faCheckCircle} color="green" /> : null}
        {leaveItem.status === "pending" ? <FontAwesomeIcon icon={faClock} color="orange" /> : null}
        {leaveItem.status === "rejected" ? <FontAwesomeIcon icon={faExclamationCircle} color="red" /> : null}
        <Text> {leaveItem.status}</Text>
      </View>

    </View>
    {leaveItem.status === "pending" ? (<View style={Style.infoBlock}>
      <TouchableOpacity style={{ flexDirection: "row", justifyContent: "center" }} onPress={() => deleteHandler(leaveItem._id)}>
        <FontAwesomeIcon icon={faTrashAlt} color="black" />
      </TouchableOpacity>
    </View>) : null}
  </View>
}

const LeaveListComponent: React.FC<Props> = ({ leaveRequestList, deleteItem, loadLeaveRequests }) => {

  React.useEffect(() => {
    loadLeaveRequests();
  }, []);

  return (
    <View style={Style.container}>

      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
        <Text style={{ fontSize: 23 }}>My leave requests</Text>
      </View>
      {!leaveRequestList.length &&<Text style={ScreenStyle.heading}>Nothing to display</Text>}
      <FlatList<Store.LeaveRequest>
        data={leaveRequestList}
        keyExtractor={(item, index) => item._id + index}
        renderItem={itemRenderer(deleteItem)}
      />

      <FAButton
        onPress={() => navigate(ScreenNames.ApplyLeaveScreen)}
      >
        <FontAwesomeIcon icon={faPlus} color={Colors.primaryForeground} />
      </FAButton>
    </View >
  )
}



const mapStateToProps = (state: Store) => ({
  leaveRequestList: state.leaveRequests.map((element: Store.LeaveRequest) => {
    return {

      ...element, startDate: new Date(+(element.startDate)), endDate: new Date(+(element.endDate))
    }
  })

})

const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => {
  // no actions defined
  return {
    deleteItem: (id: string) => dispatch({ type: ActionTypes["api/deleteLeave"], payload: { id } }),
    loadLeaveRequests: () => dispatch(actionMaker.GET_LEAVE({}))
  }
}

export const LeaveScreen = connect(
  mapStateToProps, mapDispatchToProps
)(LeaveListComponent)
