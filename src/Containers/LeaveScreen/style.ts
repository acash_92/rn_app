import { StyleSheet } from 'react-native'
import Fonts from '../../Theme/Fonts'
import {ScreenStyle} from '../../Theme/ApplicationStyles'

export const Style = StyleSheet.create({
  container: {
    ...ScreenStyle.container,
    margin: 30,
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    ...Fonts.style.h2,
    textAlign: 'center',
    marginBottom: 10,
  },
  text: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: 5,
  },
  instructions: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: 5,
    fontStyle: 'italic',
  },
  loading: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: 5,
  },
  result: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: 5,
  },
  error: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: 5,
    color: 'red',
  },
  logoContainer: {
    width: '100%',
    height: 300,
    marginBottom: 25,
  },
  logo: {
    width: '100%',
    height: '100%',
  },

// item component
  row: {
        height: 80,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "stretch",
        backgroundColor:"#cde",
        borderLeftColor:"#bcd",
        borderLeftWidth:4,
        marginBottom:1,
        padding:2
    },

    timeBlock: {
        width: "20%",
        //as container
        flexDirection:"column",
        justifyContent:"center",
        padding:2
    },
    infoBlock: {
        width: "25%",
        //as container
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent:"center",
        padding:2
    },
    areaBlock: {
        //as child
        flexGrow: 2,

        //as container
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent:"center",
        padding:2
    }
})
