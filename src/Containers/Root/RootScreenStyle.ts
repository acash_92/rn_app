import { StyleSheet } from 'react-native'
import {ScreenStyle} from '../../Theme/ApplicationStyles'

export default StyleSheet.create({
  container: {
    ...ScreenStyle.container,
  },
})
