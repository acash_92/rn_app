import * as React from 'react'
import { setTopLevelNavigator } from '../../Services/NavigationService'
import AppNavigator from '../../Navigators/AppNavigator'
import { Dispatch } from 'redux'
import { View } from 'react-native'
import styles from './RootScreenStyle'
import { connect } from 'react-redux'
import { NavigationContainerComponent } from 'react-navigation'
import { Store } from 'project-defined-types/StoreDefinitions'
import { ActionBundle } from '../../Redux/Reducers'
import { ActionTypes } from "../../Constants/ActionTypes";
// import StartupActions from '../../Redux/Stores/Startup/Actions'

interface Props {
  startup: () => void
}

class RootScreenComponent extends React.Component<Props>{

  componentDidMount() {
    // Run the startup saga when the application is starting
    this.props.startup()
  }

  render() {
    return (
      <View style={styles.container}>
        <AppNavigator
          // Initialize the NavigationService (see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html)
          ref={(navigatorRef: NavigationContainerComponent) => {
            setTopLevelNavigator(navigatorRef)
          }}
        />
      </View>
    )
  }
}


const mapStateToProps = (state: Store) => ({})

const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => ({
  startup: () => dispatch({ type: ActionTypes.APP_START_UP, payload: undefined })

})

// tslint:disable-next-line: variable-name
export const RootScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(RootScreenComponent)
