import * as React from 'react'
import { Platform, Text, View, TouchableOpacity, ListRenderItem, ListRenderItemInfo } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { Card, RoundImg, Button } from '../../Components'
import { Store } from 'project-defined-types'
import { FlatList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus, faTrashAlt, faExclamationCircle, faCheckCircle, faClock } from '@fortawesome/free-solid-svg-icons'
import { navigate } from '../../Services/NavigationService'
import { ScreenNames } from '../../Navigators/ScreenNames'
import { Colors, ScreenStyle, Images } from '../../Theme'
import { ActionTypes, actionMaker } from '../../Constants'
import { ActionBundle } from '../../Redux/Reducers'
import { Dispatch } from 'redux'

interface Props {
  leaveRequestList: Store.LeaveRequest[],
  employees: Store.User[],
  approve: (id: string) => void,
  reject: (id: string) => void,
  loadLeaveRequests: () => void
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})


const itemRenderer = (
  approve: Props["approve"],
  reject: Props["reject"],
  employees: Store.User[]
) => ({ item: leaveItem }: ListRenderItemInfo<Store.LeaveRequest>) => {
  let employee = employees.find(emp => emp._id === leaveItem.user)
  return <Card style={ScreenStyle.stack}>
    <View style={ScreenStyle.row}>
      <View style={Style.timeBlock}>
        <RoundImg source={employee?.picture || Images.logo} radius={25} />
        <Text style={{ fontSize: 18 }}>{employee?.name}</Text>
        <Text style={{ fontSize: 18 }}>{leaveItem.days}{leaveItem.days > 1 ? " days" : " day"}</Text>
      </View>
      <View style={Style.areaBlock}>
        <View>
          <Text style={{ fontSize: 18 }}>
            {leaveItem.startDate.toLocaleDateString()}
            {leaveItem.days > 1 ? " - " + leaveItem.endDate.toLocaleDateString() : ""}
          </Text>
          <Text> {leaveItem.type}</Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text> {leaveItem.comment}</Text>
          </View>
        </View>
        <View style={[ScreenStyle.row, { justifyContent: "center" }]}>
          <Button onPress={() => reject(leaveItem._id)} style={{ backgroundColor: Colors.warning }}>Reject</Button>
          <Button onPress={() => approve(leaveItem._id)} style={{ backgroundColor: Colors.success }}>Approve</Button>
        </View>
      </View>
    </View>
  </Card >
}

const PendingLeaveListComponent: React.FC<Props> = ({ leaveRequestList, employees, approve, reject, loadLeaveRequests }) => {

  React.useEffect(() => {
    loadLeaveRequests();
  }, []);

  return (
    <View style={Style.container}>

      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
        <Text style={{ fontSize: 23 }}>Pending leave requests</Text>
      </View>
      {!leaveRequestList.length && <Text style={ScreenStyle.heading}>Nothing to display</Text>}

      <FlatList<Store.LeaveRequest>
        data={leaveRequestList}
        keyExtractor={(item, index) => item._id + index}
        renderItem={itemRenderer(approve, reject, employees)}
      />
    </View >
  )
}



const mapStateToProps = (state: Store) => ({
  leaveRequestList: state.leaveRequests.map((element: Store.LeaveRequest) => {
    return {

      ...element, startDate: new Date(+(element.startDate)), endDate: new Date(+(element.endDate))
    }
  }),
  employees: state.employees

})

const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => {
  // no actions defined
  return {
    approve: (id: string) => dispatch(actionMaker.DECIDE_LEAVE({ id, status: "approved" })),
    reject: (id: string) => dispatch(actionMaker.DECIDE_LEAVE({ id, status: "rejected" })),
    loadLeaveRequests: () => {
      dispatch(actionMaker.GET_LEAVE({ status: "pending" }))
      dispatch(actionMaker.GET_EMPLOYEES())
    }
  }
}

export const PendingLeaveReqScreen = connect(
  mapStateToProps, mapDispatchToProps
)(PendingLeaveListComponent)
