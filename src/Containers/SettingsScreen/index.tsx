import * as React from 'react';
import { Platform, Text, View, TextInput, Modal, Alert, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Style } from './style';
import { Images, Colors } from '../../Theme';
import { Store } from 'project-defined-types'
import { ScrollView } from 'react-navigation';
import { ScreenStyle } from '../../Theme'
import { actionMaker } from '../../Constants/ActionTypes';
import NotificationSounds, { playSampleSound, Sound } from 'react-native-notification-sounds';
import { ActionBundle } from "../../Redux/Reducers";
import { FAButton, RoundImg, Button, Card, InfoLine, ErrorLevels } from '../../Components';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import DocumentPicker, { DocumentPickerResponse } from 'react-native-document-picker';
import { faCameraRetro } from '@fortawesome/free-solid-svg-icons';
import { ErrorKey } from '../../Constants';
// import { AWS } from 'aws-sdk/dist/aws-sdk-react-native';

interface Props {
  user?: Store.User,
  errors?: Store.UIErrors,
  waiting: Store["waiting"],
  updatePassword: (password: string, reTypedPass: string, auth: string) => void
  logout: () => void,
  updateDP: (file: DocumentPickerResponse, userId: string) => void
}

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})

// tslint:disable-next-line: no-unused-expression
const SettingsScreenComponent: React.FC<Props> = (props) => {
  if (!props.user) return <Text>Something is wrong!!</Text>
  const [email, setEmail] = React.useState(props.user.email);
  const [newPass, setNewPass] = React.useState('');
  const [rePass, setRePass] = React.useState('');
  const [currentPass, setCurrentPass] = React.useState('');
  const [selectedSound, setSoundSelection] = React.useState<null | Sound>(null);
  const [showSoundPicker, togglePicker] = React.useState(false);
  const [availableSounds, updateSoundList] = React.useState<Sound[]>([]);
  const [dp, setDP] = React.useState(props.user ? { uri: props.user.picture } : Images.logo);

  React.useEffect(() => {
    NotificationSounds.getNotifications().then((soundsList) => {
      // playSampleSound(soundsList[1]); 
      updateSoundList(soundsList);
    });
  }, []);

  const wasWaiting = React.useRef(false);
  React.useEffect(() => {
    // clear fields when waiting resolves
    if (props.waiting) {
      wasWaiting.current = true;
      return;
    }
    if (wasWaiting.current) {
      wasWaiting.current = false;
      if (props.errors) return;
      setNewPass('');
      setRePass('');
      setCurrentPass('');
      // dp
      setDP(props.user?.picture ? { uri: props.user.picture } : Images.logo)
    }

  }, [props.waiting])
  function setSound(val: null | Sound) {
    setSoundSelection(val);
  }

  async function pickImg() {
    try {
      const result = await DocumentPicker.pick({
        type: [DocumentPicker.types.images],
      });
      // console.log("PPPPP",
      //   result.uri,
      //   result.type, // mime type
      //   result.name,
      //   result.size
      // );
      setDP(result);
      if (!props.user) return;
      props.updateDP(result, props.user._id)

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  return (
    <ScrollView>
      <View style={Style.container}>
        {props.waiting && <Card><ActivityIndicator /></Card>}
        <Card>
          <Text style={ScreenStyle.heading}>Profile</Text>

          <View style={[ScreenStyle.stack, { justifyContent: "center", alignItems: "center", alignSelf: "stretch" }]}>
            {/* <View style={[ScreenStyle.row]}> */}

            <RoundImg radius={70} style={Style.dp} source={dp} resizeMode={'contain'} />
            <FAButton style={{ position: "relative", bottom: 40, left: 45, width: 50, height: 50 }}
              onPress={pickImg} disabled={props.waiting}>
              <FontAwesomeIcon icon={faCameraRetro} color={Colors.primaryForeground} size={20} />
            </FAButton>

            {/* </View> */}
            <Text style={{ fontWeight: "bold" }}>{props.user.name}
            </Text>
            <Text>{/* #00001 |  */}{props.user.designation}</Text>

            <View style={{ marginTop: "auto" }}>
              <Button onPress={props.logout} disabled={props.waiting}>Logout</Button>
            </View>
          </View>
        </Card>
        <Card>
          <Text style={ScreenStyle.heading}>Security</Text>

          <TextInput onChangeText={setEmail} value={email} placeholder="Email" editable={false}></TextInput>
          <TextInput onChangeText={setNewPass} value={newPass} placeholder="New password" secureTextEntry={true} ></TextInput>
          <TextInput onChangeText={setRePass} value={rePass} placeholder="Confirm password" secureTextEntry={true} ></TextInput>
          <Text>Current password</Text>
          <TextInput onChangeText={setCurrentPass} value={currentPass} placeholder="Current password" secureTextEntry={true} ></TextInput>
          <InfoLine level={ErrorLevels.CRITICAL} currentErrors={props.errors} show={ErrorKey.REPASSWORD_NOT_SAME} watchValue={newPass + ':' + rePass} />
          <InfoLine level={ErrorLevels.CRITICAL} currentErrors={props.errors} show={ErrorKey.CREDENTIAL_MISMATCH} />
          <Button
            onPress={() => { props.updatePassword(newPass, rePass, currentPass) }}
            disabled={!newPass || !rePass || props.waiting}
          >Update password</Button>
        </Card>
        {/*  <Card>
          <TouchableOpacity onPress={() => togglePicker(true)}>
            <Text style={ScreenStyle.heading}>Notification sound</Text>
            <TextInput value={selectedSound ? selectedSound.title : "Default"} editable={false}
              pointerEvents="none" />
          </TouchableOpacity>
          <Modal
            animationType="slide"
            transparent={true}
            visible={showSoundPicker}
            onRequestClose={() => {
              togglePicker(false)
            }}>
            <View style={[ScreenStyle.row, Style.modalContainer]}>
              <View style={[ScreenStyle.stack, Style.modalContent]}>
                <Text style={{ fontWeight: "200", fontSize: 20 }}>Notification Tones</Text>

                <View style={[ScreenStyle.stack, { flexShrink: 2 }]}>
                  <ScrollView>
                    <TouchableOpacity
                      onPress={() => { setSound(null); }}
                      style={[ScreenStyle.listItem, (selectedSound ? null : ScreenStyle.selectedItem)]}
                    >
                      <Text>Default</Text>
                    </TouchableOpacity>
                    {availableSounds.map((sound, i) => (
                      <TouchableOpacity
                        key={i}
                        onPress={() => { setSound(sound); playSampleSound(sound) }}
                        style={[ScreenStyle.listItem, selectedSound && sound.soundID === selectedSound.soundID ? ScreenStyle.selectedItem : null]}
                      >
                        <Text>{sound.title}</Text>
                      </TouchableOpacity>
                    ))}
                  </ScrollView>
                </View>
                <Button onPress={() => { togglePicker(false) }}>Set sound</Button>
              </View>
            </View>
          </Modal>
        </Card>
       */}
      </View>
    </ScrollView>
  )
}





const mapStateToProps = (state: Store) => {
  return {
    user: state.user,
    errors: state.currentErrors,
    waiting: state.waiting,
  }
}

const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined

  logout: () => dispatch(actionMaker["api/logout"]()),
  updatePassword: (pass: string, rePass: string, auth: string) => {
    if (pass === rePass) {
      dispatch(actionMaker["api/updateCurrentUser"]({ password: pass, auth }))
    } else {
      dispatch(actionMaker.SHOW_CURRENT_UI_ERROR([new Error(ErrorKey.REPASSWORD_NOT_SAME)]))
    }
  },
  updateDP: (file: DocumentPickerResponse, userId: string) => {
    dispatch(actionMaker["api/updateCurrentUser"]({ photo: { file, userId } }));
  }
})

export const SettingsScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsScreenComponent)
