import { StyleSheet } from 'react-native'
import Fonts from '../../Theme/Fonts'
import { ScreenStyle } from '../../Theme/ApplicationStyles'

export const Style = StyleSheet.create({
  container: {
    ...ScreenStyle.container,
    margin: 30
  },
  title: {
    ...Fonts.style.h2,
    textAlign: 'center',
    marginBottom: 10,
  },
  text: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: 5,
  },
  dp: {
    width: 100, height: 100, borderWidth: 1,
    borderColor: "#777777"
  },
  modalContainer: { justifyContent: "center", alignItems: "stretch", height: "100%", backgroundColor: "rgba(0,0,0,.3)" },
  modalContent: { 
    backgroundColor: "white",
    margin: 40, flexGrow: 2, 
    overflow: "hidden", 
    borderRadius: 10, 
    paddingBottom: 10, 
    paddingTop: 10, 
    justifyContent: "space-between" 
  }
})
