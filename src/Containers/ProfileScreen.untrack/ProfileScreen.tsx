import * as React from 'react'
import { Platform, Text, View, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { Style } from './style'
import { Images } from '../../Theme'
import { RoundImg, Button } from '../../Components'
import { Store } from 'project-defined-types'
import { ActionTypes, actionMaker } from "../../Constants/ActionTypes";
import { ActionBundle } from "../../Redux/Reducers";


interface Props {
  user?: Store.User,
  profileReady?: boolean,
  userErrorMessage?: string,
  logout?: () => void,
}



class ProfileScreenComponent extends React.Component<Props, {}> {


  componentDidMount() {
  }

  render() {
    return (
      <View style={Style.container}>
        {this.props.profileReady ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
            <View>
              <View style={Style.logoContainer}>
                <RoundImg  source={Images.logo} resizeMode={'contain'} radius={150} />
              </View>
              <Text style={Style.text}>To get started, edit App.js</Text>
              <Text style={Style.instructions}>"ba ba ba..."</Text>
              <Button onPress={this.props.logout}>Logout</Button>
            </View>
          )}
      </View>
    )
  }


}



const mapStateToProps = (state: Store) => ({
  user: state.user,
  waiting: state.waiting,
  userErrorMessage: ""
})

// tslint:disable-next-line: no-any
const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => ({
  // no actions defined
  // dispatch({ type: ActionTypes["api/login"], payload: { username: 'eee', name: "ssss" } });
  // fetchUser: dispatch({ type: ActionTypes["api/"], payload: { username: 'eee', name: "ssss" } });
  logout: () => dispatch(actionMaker["api/logout"]())
})

export const ProfileScreen = connect(
  mapStateToProps, mapDispatchToProps
)(ProfileScreenComponent)

export const NavDescriptor = {
  screen: ProfileScreen,
  navigationOptions: {
    title: "Profile"
  }
}