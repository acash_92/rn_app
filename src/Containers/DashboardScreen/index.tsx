import * as React from 'react'
import { Platform, Text, View, Button, ActivityIndicator, Image, SectionListData, SectionListRenderItemInfo, TouchableOpacity, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { EventItem,Dialog } from '../../Components'
import { Store } from 'project-defined-types'
import { SectionList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import DateTimePicker from '@react-native-community/datetimepicker'

interface Props {
  user?: Store.User,
  userIsLoading?: boolean,
  userErrorMessage?: string,
  fetchUser?: () => void,
  liveInEurope?: boolean,
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})

export type DataItem = string
export interface DataSection extends SectionListData<DataItem> {
  title: string,
}
const DATA: DataSection[] = [
  {
    title: 'Main dishes',
    data: ['Pizza', 'Burger', 'Risotto'],
  },
  {
    title: 'Sides',
    data: ['French Fries', 'Onion Rings', 'Fried Shrimps'],
  },
  {
    title: 'Drinks',
    data: ['Water', 'Coke', 'Beer'],
  },
  {
    title: 'Desserts',
    data: ['Cheese Cake', 'Ice Cream'],
  },
];


function Item({ data }: { data: string }) {
  return (
    <View style={{
      backgroundColor: '#009',
      padding: 20,
      marginVertical: 8,
    }}>
      <Text style={{ fontSize: 24 }}>{data}nnn</Text>
    </View>
  );
}
const itemRenderer = ({ item }: SectionListRenderItemInfo<DataItem>) => {
  return <EventItem data={item} />
}

const sectionRenderer = ({ section }: { section: SectionListData<string> }) => (
  <Text style={{ fontSize: 15, padding: 5 }}>Today 20 February 2019 - {section.title}</Text>
)

const DashboardScreen: React.FC<Props> = () => {


  function tomorrowIfNot(date?: Date | undefined) {
    const today = new Date();
    const tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1)
    return date ? new Date(date) : tomorrow;
  }

  function pickingDone(setter: (val: boolean) => void) {
    setter(Platform.OS === 'ios' ? true : false);
    return true;
  }

  function dateString(date: Date) {
    return date.toLocaleDateString()
  }

  const [startDate, setStartDate] = React.useState<Date>(tomorrowIfNot());
  const [endDate, setEndDate] = React.useState<Date>(tomorrowIfNot());
  const [showStartDatePicker, setShowStartDatePicker] = React.useState(false);
  const [showEndDatePicker, setShowEndDatePicker] = React.useState(false);
  const [showDateRange, setShowDateRange] = React.useState(false);

  return (
    <View style={Style.container}>
      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
        <Text style={{ fontSize: 23 }}>Data Log</Text>
        <TouchableOpacity onPress={() => setShowDateRange(true)}>

          <FontAwesomeIcon icon={faCalendarAlt} color="black" />
        </TouchableOpacity>
        <Dialog
          title="Set date range"
          titleIcon={faCalendarAlt}
          visible={showDateRange}
          primaryButton={["OK", () => setShowDateRange(false)]}
          onRequestClose={() => {
            setShowDateRange(false);
          }}>
          <View style={{ padding: 5 }}>
            <TouchableOpacity onPress={() => setShowStartDatePicker(!showStartDatePicker)}>
              <Text>Start Date</Text>
              <TextInput placeholder="Start Date" value={dateString(startDate)} editable={false} />
            </TouchableOpacity>

            {showStartDatePicker && <DateTimePicker value={startDate}
              mode="date"
              is24Hour={true}
              display="default"
              minimumDate={tomorrowIfNot(undefined)}
              onChange={(event, date) => pickingDone(setShowStartDatePicker) && setStartDate(tomorrowIfNot(date))} />
            }

            <TouchableOpacity onPress={() => setShowEndDatePicker(!showEndDatePicker)}>
              <Text>End Date</Text>
              <TextInput placeholder="Start Date" value={dateString(endDate)} editable={false} />
            </TouchableOpacity>

            {showEndDatePicker && <DateTimePicker value={endDate}
              mode="date"
              is24Hour={true}
              display="default"
              minimumDate={tomorrowIfNot(startDate)}
              onChange={(event, date) => pickingDone(setShowEndDatePicker) && setEndDate(tomorrowIfNot(date))} />
            }
          </View>
        </Dialog>
      </View>
      <SectionList<DataItem>
        sections={DATA}
        keyExtractor={(item, index) => item + index}
        renderItem={itemRenderer}
        renderSectionHeader={sectionRenderer}
      />
      {/* )} */}
    </View >
  )


}



const mapStateToProps = (state: Store) => ({
  user: state.user,
  userErrorMessage: "",
  liveInEurope: false,
})

// // tslint:disable-next-line: no-any
// const mapDispatchToProps = (dispatch: Dispatch<ActionBundle>) => {
//   // no actions defined
//   dispatch({ type: ActionTypes["api/login"], payload: { username: 'eee', name: "ssss" } });
// }

export default connect(
  mapStateToProps
)(DashboardScreen)
