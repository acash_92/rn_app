import * as React from 'react'
import { Platform, Text, View, Picker, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme/ApplicationStyles'
import { Store } from 'project-defined-types/StoreDefinitions'
import { ScrollView } from 'react-navigation';
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import { TextInput } from 'react-native-gesture-handler'
import { InfoLine, ErrorLevels, Card, Button } from '../../../Components'
import { ErrorKey } from '../../../Constants/ErrorKeys'
import { ThirdLayerAppbar } from 'project-defined-types'
import { ActionTypes, actionMaker } from '../../../Constants/ActionTypes'
import { ActionBundle } from '../../../Redux/Reducers';
import { Colors } from '../../../Theme'
import { ScreenNames } from '../../../Navigators/ScreenNames'

interface Props {
  currentErrors?: Store.UIErrors,
  dirtyState: Store["employeeDirty"],
  setDirty: (emp: Store["employeeDirty"]) => void,
  waiting: boolean
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})



const AddEmployee: React.FC<Props> = (props) => {
  const _id = props.dirtyState._id || "";
  const [name, setName] = React.useState(props.dirtyState.name);
  const [designation, setDesignation] = React.useState(props.dirtyState.designation);
  const [email, setEmail] = React.useState(props.dirtyState.email);
  const [phone, setPhone] = React.useState(props.dirtyState.phone || '');
  const [userType, setUserType] = React.useState<Store.Employee["userType"]>(props.dirtyState.userType);

  React.useEffect(() => {
    props.setDirty({ _id, name, designation, email, phone, userType, shouldUpdate: false });
  }, [name, designation, email, phone]);

  React.useEffect(() => {
    if (!props.dirtyState.shouldUpdate) return;
    setName(props.dirtyState.name);
    setDesignation(props.dirtyState.designation);
    setEmail(props.dirtyState.email);
    setPhone(props.dirtyState.phone || '');
    setUserType(props.dirtyState.userType);
  }, [props.dirtyState.shouldUpdate])

  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <View style={ScreenStyle.stack}>
            <Text>Full Name</Text>
            <TextInput placeholder="Full Name" value={name} onChangeText={setName} />
            <InfoLine show={ErrorKey.NAME_REQUIRED} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} />

          </View>

          <View style={ScreenStyle.stack}>
            <Text>Designation</Text>
            <TextInput placeholder="Designation" value={designation} onChangeText={setDesignation} />
            {/* <ErrorLine show={ErrorKey.EMP_NAME_REQUIRED} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} /> */}

          </View>
          {props.waiting && <ActivityIndicator size="small" color={Colors.primaryTint} />}
          {/* <View style={ScreenStyle.stack}>
            <Text>User type</Text>
            <Picker selectedValue={userType} onValueChange={setUserType} enabled={false}>
              <Picker.Item label="Admin" value="ADMIN" />
              <Picker.Item label="Employee" value="EMPLOYEE" />
            </Picker>
          </View> */}
          <View style={ScreenStyle.stack}>
            <Text>Email</Text>
            <TextInput placeholder="Email id" value={email} onChangeText={setEmail} />
            <InfoLine show={ErrorKey.EMAIL_REQUIRED} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} />
            <InfoLine show={ErrorKey.EMAIL_INVALID} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} />
            <InfoLine show={ErrorKey.EMAIL_IN_USE} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} />

          </View>
          <View style={ScreenStyle.stack}>
            <Text>Phone number</Text>
            <TextInput placeholder="Phone number" value={phone} onChangeText={setPhone} />
            <InfoLine show={ErrorKey.INVALID_PHONE} level={ErrorLevels.CRITICAL} currentErrors={props.currentErrors} />

          </View>

        </View>
      </ScrollView>
    </View >
  )
}



const mapStateToProps = (state: Store) => ({
  // area: state.areas,

  currentErrors: state.currentErrors,
  dirtyState: state.employeeDirty,
  waiting: state.waiting
})

const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  setDirty: (emp: Store["employeeDirty"]) => dispatch(actionMaker.SET_DIRTY_EMPLOYEE(emp)),
})



export const AddEmployeeScreen = connect(mapStateToProps, mapDispatchToProps)(AddEmployee)

// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const AddEmployeeScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.AddEmployeeScreen,
  screen: AddEmployeeScreen,
  menuBarButtons: [{
    icon: faCheck,
    label: "Save",
    event: (dispatch) => dispatch(actionMaker["api/saveUser"]())
  }]
}