import * as React from 'react'
import { Platform, Text, View, Picker, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../../Theme/ApplicationStyles'
import { ScrollView } from 'react-navigation';
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { TextInput } from 'react-native-gesture-handler'
import { InfoLine, ErrorLevels, Card, Button } from '../../../Components'
import { ErrorKey } from '../../../Constants/ErrorKeys'
import { ThirdLayerAppbar, Store } from 'project-defined-types'
import { ActionTypes, actionMaker } from '../../../Constants/ActionTypes'
import { ActionBundle } from '../../../Redux/Reducers';
import { Colors } from '../../../Theme'
import { ScreenNames } from '../../../Navigators/ScreenNames'
import { store } from '../../../Redux/Stores'

interface Props {
  currentErrors?: Store.UIErrors,
  dirtyState: Store["employeeDirty"],
  clearDirty: () => void,
  deleteUser: (id: string) => void,
  waiting: boolean
}


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu.',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu.',
})



const ViewEmployee: React.FC<Props> = (props) => {

  React.useEffect(() => {

    return () => {
      props.clearDirty();
    }
  }, []);

  return (

    <View style={Style.container}>
      <ScrollView>
        <View style={ScreenStyle.stack}>
          <View style={ScreenStyle.stack}>
            <Text>Full Name</Text>
            <Text>{props.dirtyState.name}</Text>

          </View>

          <View style={ScreenStyle.stack}>
            <Text>Designation</Text>
            <Text>{props.dirtyState.designation}</Text>
          </View>
          {props.waiting && <ActivityIndicator size="small" color={Colors.primaryTint} />}
          {/* <View style={ScreenStyle.stack}>
            <Text>User type</Text>
            <Picker selectedValue={userType} onValueChange={setUserType} enabled={false}>
              <Picker.Item label="Admin" value="ADMIN" />
              <Picker.Item label="Employee" value="EMPLOYEE" />
            </Picker>
          </View> */}
          <View style={ScreenStyle.stack}>
            <Text>Email</Text>
            <Text>{props.dirtyState.email}</Text>
          </View>
          <View style={ScreenStyle.stack}>
            <Text>Phone number</Text>
            <Text>{props.dirtyState.phone}</Text>

          </View>
          {!!props.dirtyState.userType && props.dirtyState.userType === "EMPLOYEE" && <Card>
            <Text style={ScreenStyle.heading}>Remove user</Text>
            <Text>This action is non-revocable</Text>
            <InfoLine
              show={ErrorKey.USER_GROUP_DEPENDENCY}
              level={ErrorLevels.CRITICAL}
              currentErrors={props.currentErrors} />
            <Button onPress={() => props.deleteUser(props.dirtyState._id)}>Remove user</Button>
          </Card>}
        </View>
      </ScrollView>
    </View >
  )
}



const mapStateToProps = (state: Store) => ({
  // area: state.areas,

  currentErrors: state.currentErrors,
  dirtyState: state.employeeDirty,
  waiting: state.waiting
})

const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  clearDirty: () => dispatch(actionMaker.CLEAR_DIRTY_EMPLOYEE()),
  deleteUser: (id: string) => dispatch(actionMaker["api/deleteUser"]({ id })),
})



export const ViewEmployeeScreen = connect(mapStateToProps, mapDispatchToProps)(ViewEmployee)

// ThirdLayerAppbar.MenubarButtonDef<ActionBundle>
export const ViewEmployeeScreenDef: ThirdLayerAppbar.ScreenDef<ScreenNames, ActionBundle> = {
  screenName: ScreenNames.ViewEmployeeScreen,
  screen: ViewEmployeeScreen,
  menuBarButtons: [
    {
      icon: faEdit,
      label: "Edit",
      event: (dispatch) => {
        const { employeeDirty } = store.getState()
        dispatch(actionMaker.EDIT_EMPLOYEE(employeeDirty))
      }
    }]
}