import * as React from 'react'
import { Platform, Text, View, ListRenderItemInfo, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { Style } from './style'
import { ScreenStyle } from '../../Theme/ApplicationStyles'
import { Store } from 'project-defined-types'
import { FlatList } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { navigate } from '../../Services/NavigationService'
import { ScreenNames } from '../../Navigators/ScreenNames'
import { FAButton } from "../../Components"
import { Colors } from '../../Theme'
import { actionMaker } from '../../Constants/ActionTypes'
import { ActionBundle } from '../../Redux/Reducers'
import { DrawerContentComponentProps } from 'react-navigation-drawer/lib/typescript/src/types'


interface Props extends DrawerContentComponentProps {
  employees: Store.Employee[],
  openToView: (emp: Store.Employee) => void,
  loadEmployees: () => void
  launchNewEmployeeScreen: () => void
}

function Item({ data, onPress }: { data: Store.Employee, onPress: (emp: Store.Employee) => void }) {
  return (
    <View style={ScreenStyle.listItem}>
      <TouchableOpacity onPress={() => onPress(data)}>
        <Text>{data.name}</Text>
      </TouchableOpacity>
    </View>
  );
}
const itemRenderer = (props: Props) => ({ item }: ListRenderItemInfo<Store.Employee>) => {
  return <Item data={item} onPress={props.openToView} />
}

const EmployeeListComponents: React.FC<Props> = (props) => {

  React.useEffect(() => {
    const subscription = props.navigation.addListener("willFocus", () => {
      props.loadEmployees();
    })
    return subscription.remove;
  }, []);

  return (
    <View style={Style.container}>

      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
        <Text style={{ fontSize: 23 }}>Employees</Text>
        {/* <FontAwesomeIcon icon={faCalendarAlt} color="black" /> */}
      </View>
      
      {!props.employees.length &&<Text style={ScreenStyle.heading}>Nothing to display</Text>}
      <FlatList
        data={props.employees}
        keyExtractor={(item) => item._id}
        renderItem={itemRenderer(props)}
      />
      {/* )} */}
      <FAButton
        onPress={props.launchNewEmployeeScreen}
      >
        <FontAwesomeIcon icon={faPlus} color={Colors.primaryForeground} />
      </FAButton>
    </View >
  )
}


const mapStateToProps = (state: Store) => {
  const employees = state.employees; /* (new Array(20)).fill(0).map((val, index) => ({
    id: "id" + index,
    name: "Test Employee",
    designation: "string",
    email: "string",
    picture: "string",
    userType: "EMPLOYEE",
  })) */;

  return { employees };
}

const mapDispatchToProps = (dispatch: React.Dispatch<ActionBundle>) => ({
  // no actions defined
  openToView: (emp: Store.Employee) => dispatch(actionMaker.VIEW_EMPLOYEE(emp)),
  loadEmployees: () => dispatch(actionMaker.GET_EMPLOYEES()),
  launchNewEmployeeScreen: () => {
    dispatch(actionMaker.CLEAR_DIRTY_EMPLOYEE())
    navigate(ScreenNames.AddEmployeeScreen);
  }
})


export const EmployeeScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeListComponents)
