import { KnownPayloads } from "project-defined-types/ActionDefinitions";
import { ActionBundle } from '../Redux/Reducers'
import { State } from "react-native-gesture-handler";
import { Store } from "project-defined-types";

export enum ActionTypes {
  "api/login" = "api/login",
  "api/logout" = "api/logout",
  "api/signup" = "api/signup",
  "api/resumeSession" = "api/resumeSession",
  "api/updateCurrentUser" = "api/updateCurrentUser",
  "api/saveUser" = "api/saveUser",
  "api/deleteUser" = "api/deleteUser",
  "api/saveGroup" = "api/saveGroup",
  "api/deleteGroup" = "api/deleteGroup",
  "api/saveArea" = "api/saveArea",
  "api/deleteArea" = "api/deleteArea",
  "api/saveLeave" = "api/saveLeave",
  "api/deleteLeave" = "api/deleteLeave",
  "api/loadArea"="api/loadArea",
  "api/loadEmployee"="api/loadEmployee",
  "api/loadGroup"="api/loadGroup",
  DECIDE_LEAVE = "DECIDE_LEAVE",
  APP_START_UP = "APP_START_UP",
  LOGIN_SUCCESS = "LOGIN_SUCCESS",
  SHOW_CURRENT_UI_ERROR = "SHOW_CURRENT_UI_ERROR",
  CLEAR_CURRENT_ERRORS = "CLEAR_CURRENT_ERRORS",
  SET_DIRTY_ADD_AREA = "SET_DIRTY_ADD_AREA",
  SET_DIRTY_EMPLOYEE = "SET_DIRTY_EMPLOYEE",
  SET_DIRTY_GROUP = "SET_DIRTY_GROUP",
  SET_DIRTY_LEAVE_REQUEST = "SET_DIRTY_LEAVE_REQUEST",
  CLEAR_DIRTY_AREA = "CLEAR_DIRTY_AREA",
  CLEAR_DIRTY_EMPLOYEE = "CLEAR_DIRTY_EMPLOYEE",
  CLEAR_DIRTY_GROUP = "CLEAR_DIRTY_GROUP",
  CLEAR_DIRTY_LEAVE_REQUEST = "CLEAR_DIRTY_LEAVE_REQUEST",
  EDIT_AREA = "EDIT_AREA",
  EDIT_EMPLOYEE = "EDIT_EMPLOYEE",
  EDIT_GROUP = "EDIT_GROUP",
  VIEW_AREA = "VIEW_AREA",
  VIEW_EMPLOYEE = "VIEW_EMPLOYEE",
  VIEW_GROUP = "VIEW_GROUP",
  WAITING_RESPONSE = "WAITING_RESPONSE",
  CLEAR_RESPONSE_WAIT = "CLEAR_RESPONSE_WAIT",
  CLEAR_SESSION = "CLEAR_SESSION",
  SET_EMPLOYEE_LIST = "SET_EMPLOYEE_LIST",
  SET_GROUP_LIST = "SET_GROUP_LIST",
  SET_AREA_LIST = "SET_AREA_LIST",
  SET_LEAVE_REQUEST_LIST = "SET_LEAVE_REQUEST_LIST",
  SET_LOG_TO_DISPLAY = "SET_LOG_TO_DISPLAY",
  GET_LOG = "GET_LOG",
  GET_EMPLOYEES = "GET_EMPLOYEES",
  GET_AREAS = "GET_AREAS",
  GET_GROUPS = "GET_GROUPS",
  GET_LEAVE = "GET_LEAVE",
  PUSH_ALERT = "PUSH_ALERT"
}
// ActionTypes.EDIT

type PayloadType<T extends KnownPayloads._unionType> = (payload: T) => ActionBundle<T>;
export type ActionMakerOverload<T> =
  T extends "api/login" ? PayloadType<KnownPayloads.Login> :
  T extends "api/updateCurrentUser" ? PayloadType<KnownPayloads.UpdateCurrentUserPayload> :
  T extends ( "api/loadUser" | "api/loadArea" | "api/loadGroup" 
    | "api/deleteUser" | "api/deleteArea" | "api/deleteGroup" | "api/deleteLeave" ) ? PayloadType<{ id: string }> :
  T extends "DECIDE_LEAVE" ? PayloadType<KnownPayloads.LeaveDecision> :
  T extends "GET_LEAVE" ? PayloadType<KnownPayloads.LeaveRequestParamas> :
  T extends "SHOW_CURRENT_UI_ERROR" ? PayloadType<KnownPayloads.UIErrors> :
  T extends "LOGIN_SUCCESS" ? PayloadType<KnownPayloads.LoginSuccess> :
  T extends "SET_DIRTY_ADD_AREA" ? PayloadType<Store.Area & { shouldUpdate: boolean }> :
  T extends "SET_DIRTY_EMPLOYEE" ? PayloadType<Store.Employee & { shouldUpdate: boolean }> :
  T extends "SET_DIRTY_GROUP" ? PayloadType<Store.Group & { shouldUpdate: boolean }> :
  T extends "SET_DIRTY_LEAVE_REQUEST" ? PayloadType<Store.LeaveRequest & { shouldUpdate: boolean }> :
  T extends "EDIT_AREA" ? PayloadType<Store.Area> :
  T extends "EDIT_EMPLOYEE" ? PayloadType<Store.Employee> :
  T extends "EDIT_GROUP" ? PayloadType<Store.Group> :  
  T extends "VIEW_AREA" ? PayloadType<Store.Area> :
  T extends "VIEW_EMPLOYEE" ? PayloadType<Store.Employee> :
  T extends "VIEW_GROUP" ? PayloadType<Store.Group> :
  T extends "SET_AREA_LIST" ? PayloadType<Store.Area[]> :
  T extends "SET_EMPLOYEE_LIST" ? PayloadType<Store.Employee[]> :
  T extends "SET_GROUP_LIST" ? PayloadType<Store.Group[]> :
  T extends "SET_AREA_LIST" ? PayloadType<Store.Area[]> :
  T extends "SET_LEAVE_REQUEST_LIST" ? PayloadType<Store.LeaveRequest[]> :
  T extends "SET_LOG_TO_DISPLAY" ? PayloadType<Store.Incident[]> :
  T extends "PUSH_ALERT" ? PayloadType<{ id: string }> :
  () => ActionBundle //default


const obj = {} as { [key in ActionTypes]: ActionMakerOverload<key> };
export const actionMaker = new Proxy(obj, {
  get: (target, prop: ActionTypes, receiver) => {
    if (Object.keys(ActionTypes).findIndex(key => key === prop) > -1) {
      return (payload: KnownPayloads._unionType, errors: [Error]): ActionBundle => {
        const action = { type: prop, payload, errors };
        // Object.setPrototypeOf( action, {dispatch(){ store.dispatch(action)}})
        return action;
      }
    } else return Reflect.get(target, prop, receiver)
  }

})