export { ActionTypes, actionMaker } from "./ActionTypes"
export { ErrorKey } from "./ErrorKeys"
export { ErrorMessages } from "./ErrorMessages"

//  Config

export const Config = {
    api_server: {
        host: "34.254.181.109",//
        port: "4001",
        disable_https: true,
        gql_path: "/gql",
        auth_token_header_field: "auth_token"
    },

    localstorage: {
        keys: {
            TOKEN: "@@TOKEN"
        }
    },
    aws: {
        s3BucketUrl: "http://wizard-irisind.s3-website-ap-southeast-1.amazonaws.com"
    }
}