import * as React from 'react'
import { Platform, Text, View, Button, ActivityIndicator, Image, ViewProps } from 'react-native'
import { ErrorKey } from '../Constants/ErrorKeys'
import { ErrorMessages } from '../Constants/ErrorMessages'
import Colors from '../Theme/Colors'
import { Store } from 'project-defined-types'

export enum ErrorLevels {
    CRITICAL,
    WARNING,
    INFO,
    SUCCESS
}

const colors = {
    [ErrorLevels.CRITICAL]: Colors.error,
    [ErrorLevels.INFO]: Colors.info,
    [ErrorLevels.SUCCESS]: Colors.success,
    [ErrorLevels.WARNING]: Colors.warning,
}
interface Props extends ViewProps {
    currentErrors: Store.UIErrors | undefined,
    show: ErrorKey,
    level: ErrorLevels,
    matchPattern?: Array<string | number>,
    // tslint:disable-next-line: no-any
    watchValue?: any
}


function matchErrorMap(pattern: Array<string | number>, matchTo: Array<string | number>) {

    return (pattern.every((patternItem, index) => patternItem === matchTo[index]))
}

export const InfoLine: React.FC<Props> = (props) => {
    const [valChanged, setValChanged] = React.useState(false);



    React.useEffect(() => {
        if (!props.currentErrors) return;
        if (props.currentErrors.length) {
            // this will apply only after the current render, i.e when current error is set, it'll display the error.
            // for subsequent re-renders, it will hide. The subsequent re-renders happen when any props changes.
            setValChanged(true)
        }
    }, [props.watchValue])

    if (!props.currentErrors) return null;
    if (valChanged && (!props.currentErrors || !props.currentErrors.length)) {
        setValChanged(false)
    }


    if (valChanged) return null;
    const shouldDisplay = props.currentErrors.some(error => {
        if (error.message === props.show) {
            if (props.matchPattern) {
                if (!error.mapToParams) return;
                return matchErrorMap(error.mapToParams, props.matchPattern)
            } else return true;
        } else return false
    })
    if (shouldDisplay) {
        return (<View>
            <Text style={{ color: colors[props.level] }}>{ErrorMessages[props.show]}</Text>
            {props.children}
        </View>)
    }
    return null
}

export const GeneralErrorLines: React.FC<{ currentErrors: Store.UIErrors | undefined }> = ({ currentErrors }) => {

    return <>
        <InfoLine
            show={ErrorKey.NETWORK_ERROR}
            level={ErrorLevels.CRITICAL}
            currentErrors={currentErrors} />
        <InfoLine
            show={ErrorKey.API_ERROR}
            level={ErrorLevels.CRITICAL}
            currentErrors={currentErrors} />
    </>
}