import { StyleSheet } from 'react-native'
import { Colors } from '../../Theme'
export const Style = StyleSheet.create({
    modalContainer: { justifyContent: "center", alignItems: "stretch", height: "100%", backgroundColor: "rgba(0,0,0,.3)" },
    modalContent: {
        backgroundColor: "white",
        margin: 40, flexGrow: 2,
        overflow: "hidden",
        borderRadius: 10,
        paddingBottom: 10,
        paddingTop: 10,
        justifyContent: "space-between"
    },
    previewContainer:{
        backgroundColor:"white",
        borderColor: Colors.edges,
        borderWidth:1
    }
})