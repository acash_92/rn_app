import * as React from 'react';
import { Platform, Text, View, TextInput, TouchableOpacity, Button, Modal, ScrollView, ViewStyle, TextStyle, ListRenderItemInfo } from 'react-native'
import { Style } from './style'
import { ScreenStyle } from '../../Theme/ApplicationStyles'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBell, faCheckCircle, faSearch } from '@fortawesome/free-solid-svg-icons'
import { AllowedKeys } from 'project-defined-types';
import { Dialog } from '../Dialog'
import { FlatList } from 'react-native-gesture-handler';


/**
 * <T> is the type of data passed in,
 * <K> represents the property used to uniquely identify the data items. Remember that this is only for type assertion
 * You will have to provide the {idKey} property again. I don't know if typescript has a better way to do this.
 * What it does is simply checks if typeof {idkey} matches with that of members in selection[] 
 */
interface Props<T extends {}, K extends keyof T> {
    title?: string
    data: T[],
    idKey: K,
    labelKey: AllowedKeys<T, string>,
    selection: Array<T[K]>,
    onChange?: (vals: T[]) => void,
    previewContainerStyle?: ViewStyle,
    previewItemStyle?: TextStyle,
    disabled?: boolean,
    emptySelectionPlaceholder?: string,
    secondLineKey?: AllowedKeys<T, string>,

}


function itemRenderer<T>(itemPressHandler: Function, isSelected: (index: number) => boolean, labelKey: AllowedKeys<T, string>, subKey?: AllowedKeys<T, string>) {
    return ({ item, index }: ListRenderItemInfo<T>) => {
        return <View style={[ScreenStyle.listItem, isSelected(index) ? ScreenStyle.selectedItem : null]}>
            <TouchableOpacity onPress={() => itemPressHandler(index)}>
                <Text>{item[labelKey]}</Text>
                {!!subKey && <Text style={ScreenStyle.subText}>{item[subKey]}</Text>}
            </TouchableOpacity>
        </View>
    }
}


export const MultiSelect = <T, IDKEY extends keyof T>(props: Props<T, IDKEY>) => { // comma in generic is to trick TS into reading <T,> as type def and not a JSX tag

    const { title, data, onChange, selection, idKey, labelKey } = props;
    const { previewContainerStyle, previewItemStyle } = props;
    function mapSelection() {
        const selectionArray: number[] = [];
        selection.forEach(selectVal => {
            data.forEach((dataVal, index) => {
                if (selectVal === dataVal[idKey]) selectionArray.push(index)
            })
        });
        return selectionArray;
    }
    // const [selectionPreview, setSelectionPreview] = React.useState('Nothing selected');
    const [showModal, toggleModal] = React.useState(false);
    const [selectionState, updateSelection] = React.useState<number[]>(mapSelection())
    const [filter, updateFilter] = React.useState<string>()

    React.useEffect(() => {
        updateSelection(mapSelection())
    }, [props.selection])

    function toggleSelection(position: number) {

        let newSelection;
        if (isSelected(position)) {
            newSelection = selectionState.filter(val => val !== position)
            updateSelection(newSelection)
            // if (!newSelection.length) return setSelectionPreview("Nothing selected");
            return;
        } else {
            newSelection = [...selectionState, position]
            updateSelection(newSelection)
        }
        const firstItem = data[newSelection[0]];

        // let selectionPreview = firstItem[labelKey] as string;
        // if (newSelection.length > 1) selectionPreview += "and " + (newSelection.length - 1) + "more";
        // setSelectionPreview(selectionPreview);

    }
    function isSelected(position: number) {
        return selectionState.indexOf(position) !== -1;
    }

    function endSelection() {
        returnValues();
        toggleModal(false);
    }
    function returnValues() {
        if (onChange) {
            onChange(data.filter((item, index) => isSelected(index)))
        }
    }

    function filteredData() {
        if (!filter) return data;
        return data.filter(item => {
            const pattern = new RegExp(filter);
            return pattern.test(item[labelKey] as unknown as string)
        })
    }

    return <View>
        <TouchableOpacity onPress={() => toggleModal(true)} disabled={props.disabled}>
            <View style={[ScreenStyle.row, ScreenStyle.floatContainer, Style.previewContainer, previewContainerStyle]}>
                {selectionState.length ? selectionState.map(pos => (
                    <Text numberOfLines={1}
                        style={[ScreenStyle.floatItem, previewItemStyle]}
                        key={pos} >
                        {data[pos][labelKey]}
                    </Text>
                )) : <Text>{props.emptySelectionPlaceholder || "Nothing selected"}</Text>}
            </View>
            {/* <TextInput value={selectionPreview} editable={false}
                pointerEvents="none" /> */}
        </TouchableOpacity >
        <Dialog
            title={title || "Select"}
            primaryButton={["Done", endSelection]}
            visible={showModal}
            onRequestClose={() => {
                endSelection();
            }}>
            <View style={ScreenStyle.row}>
                <TextInput placeholder="Sreach" onChangeText={updateFilter} />
                {/* <TouchableOpacity
                    key={i}
                    onPress={updateFilter}>

                    <FontAwesomeIcon icon={faSearch} />
                </TouchableOpacity> */}
            </View>
            <FlatList
                data={filteredData()}
                keyExtractor={(item) => item[idKey] as unknown as string}
                renderItem={itemRenderer(toggleSelection, isSelected, labelKey, props.secondLineKey)} />
            {/* {data.map((val, i) => (
                <TouchableOpacity
                    key={i}
                    onPress={() => { toggleSelection(i); }}
                    style={[ScreenStyle.listItem, isSelected(i) ? ScreenStyle.selectedItem : null]}>
                    <Text>{val[labelKey]}</Text>
                    {!!props.secondLineKey && <Text style={ScreenStyle.subText}>{val[props.secondLineKey]}</Text>}
                </TouchableOpacity>
            ))} */}

        </Dialog>
    </View >;
}