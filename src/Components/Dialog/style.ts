import { StyleSheet } from 'react-native'
import { Colors } from '../../Theme'
export const Style = StyleSheet.create({
    modalContainer: { justifyContent: "center", alignItems: "center", height: "100%", backgroundColor: "rgba(0,0,0,.3)" },
    modalContent: {
        backgroundColor: "white",
        margin: 40, flexGrow: 2,
        overflow: "hidden",
        borderRadius: 10,
        padding: 0,
        paddingBottom: 10,
        justifyContent: "space-between"
    }
})