import * as React from 'react';
import { Platform, Text, View, TextInput, TouchableOpacity, Button, Modal, ScrollView, ViewStyle, TextStyle } from 'react-native'
import { Style } from './style'
import { ScreenStyle, AppBarStyle } from '../../Theme/ApplicationStyles'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { IconDefinition } from '@fortawesome/free-solid-svg-icons'
import { Colors } from '../../Theme';


/**
 * <T> is the type of data passed in,
 * <K> represents the property used to uniquely identify the data items. Remember that this is only for type assertion
 * You will have to provide the {idKey} property again. I don't know if typescript has a better way to do this.
 * What it does is simply checks if typeof {idkey} matches with that of members in selection[] 
 */
interface Props {
    title?: string
    onRequestClose?: () => void
    primaryButton?: [string, () => void]
    secondaryButton?: [string, () => void]
    visible: boolean,
    titleIcon?: IconDefinition
}

export const Dialog: React.FC<Props> = (props) => { // comma in generic is to trick TS into reading <T,> as type def and not a JSX tag

    const { title, visible, titleIcon } = props;


    return <Modal
        animationType="slide"
        transparent={true}
        visible={visible}
        onRequestClose={props.onRequestClose}>
        <View style={[ScreenStyle.row, Style.modalContainer]}>
            <View style={[ScreenStyle.stack, Style.modalContent]}>
                <View style={[ScreenStyle.row, AppBarStyle.container]}>
                    <View style={[ScreenStyle.row,{justifyContent:"flex-start", alignItems:"center"}]}>
                        {titleIcon && <FontAwesomeIcon icon={titleIcon} color={Colors.primaryForeground} />}
                        <Text style={{ fontWeight: "200", fontSize: 20, color: Colors.primaryForeground, marginLeft:10 }}>{title}</Text>
                    </View>
                </View>

                <View style={[ScreenStyle.stack, { flexShrink: 2 }]}>
                    <ScrollView>
                        {props.children}
                    </ScrollView>
                </View>
                <View style={[ScreenStyle.row, { justifyContent: "flex-end", paddingEnd: 15 }]}>

                    {props.primaryButton && <Button
                        color={Colors.primaryBackground}
                        title={props.primaryButton[0]}
                        onPress={props.primaryButton[1]} />}

                    {props.secondaryButton && <Button
                        color={Colors.secondaryBackground}
                        title={props.secondaryButton[0]}
                        onPress={props.secondaryButton[1]} />}
                </View>
            </View>
        </View>
    </Modal>;
}