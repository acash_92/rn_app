import { View, StyleSheet, ViewProps } from "react-native"
import * as React from 'react';
import { Colors } from "../Theme"

interface Props extends ViewProps {
    orientation?: "vertical" | "horizontal"
}
export const Card: React.FC<Props> = (props) => {
    const { children, style, orientation, ...restProps } = props;

    return <View
        style={[cardStyle.card, style, {
            flexDirection: orientation === "horizontal" ? "row" : "column",
            alignItems: orientation === "horizontal" ? "flex-start" : "stretch",
        }]}
        {...restProps}
    >
        {props.children}
    </View>
}

const cardStyle = StyleSheet.create({
    card: {
        minHeight: 50,
        backgroundColor: Colors.selectionHighlight1,
        borderRadius: 5,
        borderColor: Colors.edges,
        display: "flex",
        justifyContent: "space-evenly",
        padding: 5,
        marginBottom: 15
    },

})
