import * as React from 'react';
import { View, Image, ImageProps } from 'react-native'
import { Style } from './style'
import { Colors } from '../../Theme';

interface Props extends ImageProps {
    radius: number
}

export const RoundImg: React.FC<Props> = (props) => {

    const { radius, style, ...passOnProps } = props;
    const roundingStyle = {
        borderTopLeftRadius: radius,
        borderTopRightRadius: radius,
        borderBottomRightRadius: radius,
        borderBottomLeftRadius: radius,
        width: radius * 2,
        height: radius * 2
    };

    // if (typeof radius === "number") {
    //     roundingStyle.borderTopLeftRadius = roundingStyle.borderTopRightRadius = roundingStyle.borderBottomRightRadius = roundingStyle.borderBottomLeftRadius = radius
    // } else {
    //     Object.assign(roundingStyle, radius);
    // }

    return <View style={[{ borderColor: Colors.edges}, style, Style.roundPicContainer, roundingStyle]}>
        <Image {...passOnProps} style={Style.dp} />
    </View>
}