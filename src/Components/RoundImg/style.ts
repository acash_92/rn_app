import { StyleSheet } from 'react-native'
export const Style = StyleSheet.create({
    roundPicContainer: {
        overflow: "hidden",
    },
    dp: { width: "100%", height: "100%" },
})