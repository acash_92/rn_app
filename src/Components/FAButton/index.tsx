import { TouchableOpacity, StyleSheet, ViewProps, TouchableOpacityProps } from "react-native"
import * as React from 'react';
import { Colors } from "../../Theme"

export const FAButton: React.FC<TouchableOpacityProps> = (props) => {
    const { children, style, ...restProps } = props
    return <TouchableOpacity style={[faBUttonStyle.faButton, style]} {...restProps}>
        {children}
    </TouchableOpacity>
}

const faBUttonStyle = StyleSheet.create({
    faButton: {
        borderWidth: 1,
        borderColor: Colors.edges,
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 70,
        backgroundColor: Colors.primaryBackground,
        borderRadius: 100,
    }
})