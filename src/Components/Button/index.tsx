import { TouchableOpacity, StyleSheet, Text, TouchableOpacityProps, TextStyle, ActivityIndicator } from "react-native"
import * as React from 'react';
import { Colors } from "../../Theme"

interface Props extends TouchableOpacityProps {
    textStyle?: TextStyle,
    waiting?: boolean
}
export const Button: React.FC<Props> = (props) => {
    const { children, style, disabled, waiting, ...restProps } = props;
    const shouldDisable = disabled || waiting;
    return <TouchableOpacity
        style={[buttonStyle.button, shouldDisable && buttonStyle.disbledButton, style]}
        disabled={shouldDisable}
        {...restProps}
    >
        <Text style={[buttonStyle.text, style]}>{children} </Text>
        {waiting && <ActivityIndicator size="small" color={Colors.primaryForeground} />}
    </TouchableOpacity>
}

const buttonStyle = StyleSheet.create({
    button: {
        height: 50,
        backgroundColor: Colors.primaryBackground,
        borderRadius: 7,
        display: "flex",
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignSelf: "flex-end",
        paddingLeft: 20,
        paddingRight: 20,
        margin: 10
    },
    disbledButton: {
        backgroundColor: Colors.disabledBgColor
    },
    text: {
        color: Colors.primaryForeground,
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 18,
    }
})