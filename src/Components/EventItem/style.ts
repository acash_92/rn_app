import { StyleSheet } from 'react-native'
export const Style = StyleSheet.create({
    row: {
        height: 80,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "stretch",
        backgroundColor:"#cde",
        borderLeftColor:"#bcd",
        borderLeftWidth:4,
        marginBottom:1,
        padding:2
    },

    timeBlock: {
        width: "20%",
        //as container
        flexDirection:"column",
        justifyContent:"center",
        padding:2
    },
    infoBlock: {
        width: "35%",

        //as container
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent:"center",
        padding:2
    },
    areaBlock: {
        //as child
        flexGrow: 2,

        //as container
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent:"center",
        padding:2
    }
})