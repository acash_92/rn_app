import * as React from 'react';
import { Platform, Text, View } from 'react-native'
import { Style } from './style'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBell, faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { DataItem } from '../../Containers/DashboardScreen'

interface Props {
    data: DataItem
}

export const EventItem: React.FC<Props> = ({ data }) => {
    return <View style={Style.row}>
        <View style={Style.timeBlock}>
            <Text style={{ fontSize: 18 }}>09:30</Text>
        </View>
        <View style={Style.areaBlock}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <FontAwesomeIcon icon={faBell} color="black" />
                <Text> Alarm From</Text>
            </View>
            <Text>{"{AreaName}"} {data}</Text>
        </View>
        <View style={Style.infoBlock}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
                <FontAwesomeIcon icon={faCheckCircle} color="black" />
                <Text>Acknowledged</Text>
            </View>
            <Text>{"{Employee}"}</Text>
        </View>
    </View>
}